Đề tài đồ án và thực tập tốt nghiệp:
Xây dựng ứng dụng KaChat - messenger trò chuyện trực tuyến trên website

Create by : KamiTeam - Nguyễn Văn Phong


# KamiTeam-Chat-Template-Frontend

Trong Ứng dụng này có sử dụng những thư viện phía client :

- bootstrap: **^3.3.7**
- font-awesome **^4.7.0**
- jquery **^3.3.1**
- AlertifyJS **^1.11.0**
- jquery.nicescroll **^3.7.6**
- moment **^2.21.0**
- emojionearea **^3.0.0**
- peerjs **^0.3.14**
- sweetalert2 **^7.33.1**
- photoset-grid **^1.0.1**
- jquery-colorbox **^1.6.4**


"A little bit of fragrance always clings to the hands that gives you roses!"

//Thêm vào leftSide.js khi tạo được cuộc trò chuyện nhóm khi hiển thị tên nhóm dài hơn 15 kí tự thì sẽ cắt bớt đi chỉ lấy 12 kí tự thôi
<!-- <% if( conversation.name===null||conversation.name.length >= 15){ %>
                                        <%= conversation.name.substr(0,11); %> <span>...</span>
                                    <% } else { %>
                                        <%= conversation.name %>
                                    <% } %> -->


<!-- Trường hợp tài khoản chưa có bạn bè thì hiện màn hình dưới. --> trong file rightSide.js
    <!-- <div class="right active-chat" data-chat="person-default">
        <div class="top">
            <span>To: <span class="name">KamiPhong (admin)</span></span>
            <span class="chat-menu-right">
                <a href="#">
                    Tệp đính kèm
                    <i class="fa fa-paperclip"></i>
                </a>
            </span>
            <span class="chat-menu-right">
                <a href="javascript:void(0)">&nbsp;</a>
            </span>
            <span class="chat-menu-right">
                <a href="#">
                    Hình ảnh
                    <i class="fa fa-photo"></i>
                </a>
            </span>
        </div>
        <div class="content-chat">
            <div class="chat active-chat" data-chat="person-default">
                <div class="conversation-start">
                    <span>Một tháng trước</span>
                </div>
                <div class="bubble you">
                    Xin chào <strong>Nguyễn Phong</strong>, tài khoản của bạn đã sẵn sàng.
                </div>
                <div class="bubble you">
                    Bây giờ, hãy tìm kiếm bạn bè để trò chuyện.
                </div>
                <div class="bubble you">
                    Lưu ý: Đây là tin nhắn tự động, vui lòng không trả lời lại. Cảm ơn <strong>Nguyễn Phong</strong> !!!
                </div>
            </div>
        </div>

        <div class="write">
            <input type="text" class="write-chat" data-chat="" value="Hiện tại không thể Chat - Hãy tìm bạn bè trước khi chat. Xin cảm ơn !!!">
            <div class="icons">
                <a href="#"><i class="fa fa-smile-o"></i></a>
                <a href="#"><i class="fa fa-photo"></i></a>
                <a href="#"><i class="fa fa-paperclip"></i></a>
                <a href="#"><i class="fa fa-video-camera"></i></a>
            </div>
        </div>
    </div> -->


<!-- Server chạy peerjs  -->
<!-- https://kamiteam-peerjs-server.herokuapp.com/ -->

Một số hướng dẫn khi chạy dự án trên các máy khác

npm install
