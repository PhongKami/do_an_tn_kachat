export const transValidation={
    email_incorrect:"Email phải có dạng example@gmail.com!",
    gender_incorrect:"Lạ nhỉ sao trường giới tính lại sai?",
    password_incorrect:"Mật khẩu phải chứa ít nhất 8 kí tự, bao gồm chữ hoa, chữ thường, chữ số và kí tự đặc biệt!",
    password_confirmation_incorrect:"Nhập lại mật khẩu chưa chính xác!",
    update_username:"User name giới hạn từ 3 đến 17 kí tự và không được phép chứa kí tự đặc biệt",
    update_gender:"Oops! Dữ liệu giới tính có vấn đề! Bạn không nên tác động vào hệ thống - là tôi đây! T.T",
    update_address:"Địa chỉ giới hạn trong khoảng từ 3 đến 30 kí tự!",
    update_phone:"Số điện thoại của Việt Nam bắt đầu bằng chữ số 0, giới hạn trong khoảng 10-11 kí tự!",
    keyword_find_user:"Lỗi từ khóa tìm kiếm! Chỉ cho phép sử dụng kí tự chữ, số và khoảng cách! ",
    message_text_emoji_incorrect: "Tin nhắn không hợp lệ! Đảm bảo tin nhắn có 1 ký tự, tôí đa 400 kí tự!",
    add_new_group_users_incorrect: "Vui lòng chọn bạn bè để thêm vào nhóm, tối thiểu hai người!",
    add_new_group_name_incorrect: "Vui lòng nhập tên cuộc trò chuyện từ 5 đến 30 kí tự và không chứa kí tự đặc biệt!",

};

export const transErrors={
    account_in_use:"Email đã được sử dụng!",
    account_removed:"Tài khoản này đã bị gỡ khỏi hệ thống!",
    account_not_active:"Email đã được đăng kí nhưng chưa active tài khoản, vui lòng kiểm tra lại email của bạn!",
    account_undefined:"Tài khoản này không tồn tại",
    token_undifined:"Token không tồn tại. Vì bạn đã active tài khoản rồi!<br/> Mời bạn vào vương quốc nhé!",
    login_failed:"Sai tài khoản hoặc mật khẩu!",
    server_error:"Có lỗi ở phía server, vui lòng liên hệ với bộ phận hỗ trợ của chúng tôi để báo lỗi này! Xin cảm ơn!",
    avatar_type:"Kiểu file không hợp lệ, chỉ chấp nhận jpg, png.",
    avatar_size:"Ảnh upload tối đa cho phép là 1MB",
    user_current_password_failed:"Mật khẩu hiện tại không chính xác!",
    conversation_not_found: "Cuộc trò chuyện không tồn tại!",
    image_message_type:"Kiểu file không hợp lệ, chỉ chấp nhận jpg, png.",
    image_message_size:"Ảnh upload tối đa cho phép là 1MB",
    attachment_message_size:"Tệp tin đính kèm upload tối đa cho phép là 1MB"
}

export const transSuccess={
    userCreated : (userEmail)=>{
        return `Tài khoản <strong>${userEmail}</strong>. Vui lòng kiểm tra lại email của bạn để active tài khoản trước khi đăng nhập. Xin cảm ơn!`
    },
    account_actived:"Kích hoạt tài khoản thành công! Bạn đã có thể vào vương quốc - KACHAT!",
    login_success:(userName)=>{
        return `Xin chào ${userName}. Chúc bạn một ngày tốt lành! =))`
    },
    logout_success: "Đăng xuất tài khoản thành công! Hẹn gặp lại bạn! =))",
    avatar_updated:"Cập nhật ảnh đại diện thành công!",
    user_info_updated:"Cập nhật thông tin người dùng thành công!",
    user_password_updated:"Cập nhật mật khẩu thành công!"
}

export const transMail={
    subject:"KamiTeam - KaChat: Xác nhận kích hoạt tài khoản!",
    template:(linkVerify)=>{
        return `
        <h2>Bạn đã nhận được email này vì đã đăng kí tài khoản trên ứng dụng KaChat.</h2>
        <h2>Vui lòng click vào liên kết bên dưới để xác nhận kích hoạt tài khoản.</h2>
        <h3><a href="${linkVerify}" target="blank">${linkVerify}</a></h3>
        <h2>Nếu tin rằng email này là nhầm lẫn, hãy bỏ qua nó.Trân trọng.</h2>
        `;
    },
    send_failed: "Có lỗi trong quá trình gửi email. Vui lòng liên hệ lại với bộ phận hỗ trợ!"
}
