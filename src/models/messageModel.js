import mongoose from "mongoose";
const Schema = mongoose.Schema;

const MessageSchema = new Schema({
    senderId: String,
    receiverId: String,
    conversationType: String,
    messageType: String,
    sender: {
        id: String,
        name: String,
        avatar: String
    },
    receiver: {
        id: String,
        name: String,
        avatar: String
    },
    text: String,
    file: { data: Buffer, contentType: String, fileName: String },
    createdAt: { type: Number, default: Date.now },
    updatedAt: { type: Number, default: null },
    deletedAt: { type: Number, default: null }
});

MessageSchema.statics = {
    /**=================
     * Create new Message
     * Creare By : KamiTeam-Nguyễn Văn Phong
     * @param {object} item 
     */
    createNew(item) {
        return this.create(item);
    },
    /**
     * Get message item of péonal
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} senderId 
     * @param {string} receiverId 
     * @param {number} limit 
     */
    getMessagesInPersonal(senderId, receiverId, limit) {
        return this.find({
            $or: [{
                $and: [
                    { "senderId": senderId },
                    { "receiverId": receiverId }
                ]
            },
            {
                $and: [
                    { "senderId": receiverId },
                    { "receiverId": senderId }
                ]
            }
            ]
        }).sort({ "createdAt": -1 }).limit(limit).exec();
    },
    /**
     * Get message in group
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} receiverId id of group chat
     * @param {number} limit 
     */
    getMessagesInGroup(receiverId, limit) {
        return this.find({ "receiverId": receiverId }).sort({ "createdAt": -1 }).limit(limit).exec();
    },
    /**
     * Get message item of péonal
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} senderId 
     * @param {string} receiverId 
     * @param {number} skip 
     * @param {number} limit 
     */
    readMoreMessagesInPersonal(senderId, receiverId, skip, limit) {
        return this.find({
            $or: [{
                $and: [
                    { "senderId": senderId },
                    { "receiverId": receiverId }
                ]
            },
            {
                $and: [
                    { "senderId": receiverId },
                    { "receiverId": senderId }
                ]
            }
            ]
        }).sort({ "createdAt": -1 }).skip(skip).limit(limit).exec();
    },
    /**
     * Get more message in group
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} receiverId id of group chat
     * @param {number} skip 
     * @param {number} limit 
     */
    getMessagesInGroup(receiverId, skip, limit) {
        return this.find({ "receiverId": receiverId }).sort({ "createdAt": -1 }).skip(skip).limit(limit).exec();
    },
}

const MESSAGE_CONVERSATION_TYPE = {
    PERSONAL: "personal",
    GROUP: "group"
};

const MESSAGE_TYPE = {
    TEXT: "text",
    IMAGE: "image",
    FILE: "file"
}

module.exports = {
    model: mongoose.model("message", MessageSchema),
    conversationTypes: MESSAGE_CONVERSATION_TYPE,
    messageTypes: MESSAGE_TYPE
};
