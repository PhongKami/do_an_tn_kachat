import mongoose from "mongoose";
const Schema = mongoose.Schema;

const ContactSchema = new Schema({
    userId: String,
    contactId: String,
    status: { type: Boolean, default: false },
    createdAt: { type: Number, default: Date.now },
    updatedAt: { type: Number, default: null },
    deletedAt: { type: Number, default: null }
});

ContactSchema.statics = {
    /**=================
     * Create new Contact
     * Creare By : KamiTeam-Nguyễn Văn Phong
     * @param {*} item 
     */
    createNew(item) {
        return this.create(item);
    },
    /**=================
     * Find all items that related with user
     * Creare By : KamiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     */
    findAllByUser(userId) {
        return this.find({
            $or: [
                { "userId": userId },
                { "contactId": userId }
            ]
        }).exec();
    },
    /**
     * Check exist of 2 users
     * @param {string} userId 
     * @param {string} contactId 
     */
    checkExists(userId, contactId) {
        return this.findOne({
            $or: [
                {
                    $and: [
                        { "userId": userId },
                        { "contactId": contactId }
                    ]
                },
                {
                    $and: [
                        { "userId": contactId },
                        { "contactId": userId }
                    ]
                }
            ]
        }).exec();
    },
    /**
     * Remove contact
     * @param {string} userId 
     * @param {string} contactId 
     */
    removeContact(userId, contactId) {
        return this.remove({
            $or: [
                {
                    $and: [
                        { "userId": userId },
                        { "contactId": contactId },
                        { "status": true }
                    ]
                },
                {
                    $and: [
                        { "userId": contactId },
                        { "contactId": userId },
                        { "status": true }
                    ]
                }
            ]
        }).exec();
    },

    /**
     * Remove request contact sent 
     * @param {string} userId 
     * @param {string} contactId 
     */
    removeRequestContactSent(userId, contactId) {
        return this.remove({
            $and: [
                { "userId": userId },
                { "contactId": contactId },
                { "status": false }
            ]
        }).exec();
    },
    /**
    * Remove request contact received
    * @param {string} userId 
    * @param {string} contactId 
    */
    removeRequestContactReceived(userId, contactId) {
        return this.remove({
            $and: [
                { "contactId": userId },
                { "userId": contactId },
                { "status": false }
            ]
        }).exec();
    },
    /**
     * Approve request contact received
     * @param {string : of currentUser} userId 
     * @param {string} contactId 
     */
    approveRequestContactReceived(userId, contactId) {
        return this.update({
            $and: [
                { "contactId": userId },
                { "userId": contactId },
                { "status": false }
            ]
        }, {
            "status": true,
            "updatedAt": Date.now()
        }).exec();
    },
    /**
     * Get contact by userId and limit
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     * @param {number} limit 
     */
    getContacts(userId, limit) {
        return this.find({
            $and: [
                {
                    $or: [
                        { "userId": userId },
                        { "contactId": userId }
                    ]
                },
                { "status": true }
            ]
        }).sort({ "updatedAt": -1 }).limit(limit).exec();
    },
    /**
     * Get contact sent by userId and limit
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     * @param {number} limit 
     */
    getContactsSent(userId, limit) {
        return this.find({
            $and: [
                { "userId": userId },
                { "status": false }
            ]
        }).sort({ "createdAt": -1 }).limit(limit).exec();
    },
    /**
     * Get contact received by userId and limit
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     * @param {number} limit 
     */
    getContactsReceived(userId, limit) {
        return this.find({
            $and: [
                { "contactId": userId },
                { "status": false }
            ]
        }).sort({ "createdAt": -1 }).limit(limit).exec();
    },

    /**
     * Count All contact by userId
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     */
    countAllContacts(userId) {
        return this.count({
            $and: [
                {
                    $or: [
                        { "userId": userId },
                        { "contactId": userId }
                    ]
                },
                { "status": true }
            ]
        }).exec();
    },
    /**
     * Count All contact sent by userId 
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     */
    countAllContactsSent(userId) {
        return this.count({
            $and: [
                { "userId": userId },
                { "status": false }
            ]
        }).exec();
    },
    /**
     * Count All contact received by userId 
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     */
    countAllContactsReceived(userId) {
        return this.count({
            $and: [
                { "contactId": userId },
                { "status": false }
            ]
        }).exec();
    },
    /**
     * Read more contact by userId, skip, limit
     * Create By : KAmiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     * @param {number} skip 
     * @param {number} limit 
     */
    readMoreContacts(userId, skip, limit) {
        return this.find({
            $and: [
                {
                    $or: [
                        { "userId": userId },
                        { "contactId": userId }
                    ]
                },
                { "status": true }
            ]
        }).sort({ "updatedAt": -1 }).skip(skip).limit(limit).exec();
    },
    /**
     * Read more contact sent by userId, skip, limit
     * Create By : KAmiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     * @param {number} skip 
     * @param {number} limit 
     */
    readMoreContactsSent(userId, skip, limit) {
        return this.find({
            $and: [
                { "userId": userId },
                { "status": false }
            ]
        }).sort({ "createdAt": -1 }).skip(skip).limit(limit).exec();
    },
    /**
     * Read more contact sent by userId, skip, limit
     * Create By : KAmiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     * @param {number} skip 
     * @param {number} limit 
     */
    readMoreContactsReceived(userId, skip, limit) {
        return this.find({
            $and: [
                { "contactId": userId },
                { "status": false }
            ]
        }).sort({ "createdAt": -1 }).skip(skip).limit(limit).exec();
    },
    /**
     * update contact (chat personal) when has new message
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} userId curent user id
     * @param {string} contactId contact id
     */
    updateWhenHasNewMessage(userId, contactId) {
        return this.update({
            $or: [
                {
                    $and: [
                        { "userId": userId },
                        { "contactId": contactId }
                    ]
                },
                {
                    $and: [
                        { "userId": contactId },
                        { "contactId": userId }
                    ]
                }
            ]
        },{
            "updatedAt": Date.now()
        }).exec();
    },
     /**
     * Get contact friends by userId 
     * Create By : KamiTeam-Nguyễn Văn Phong
     * @param {string} userId 
     */
    getFriends(userId) {
        return this.find({
            $and: [
                {
                    $or: [
                        { "userId": userId },
                        { "contactId": userId }
                    ]
                },
                { "status": true }
            ]
        }).sort({ "updatedAt": -1 }).exec();
    },


};

module.exports = mongoose.model("contact", ContactSchema);
