import authSevice from "./authSevice";
import userSevice from "./userSevice";
import contactService from "./contactService";
import notificationSevice from "./notificationSevice";
import messageSevice from "./messageSevice";
import groupChatService from "./groupChatService";

export const auth = authSevice;
export const user = userSevice;
export const contact = contactService;
export const notification = notificationSevice;
export const message = messageSevice;
export const groupChat = groupChatService;


