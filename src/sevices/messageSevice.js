import ContactModel from "./../models/contactModel";
import UserModel from "./../models/userModel";
import ChatGroupModel from "./../models/chatGroupModel";
import MessageModel from "./../models/messageModel";
import _ from "lodash";
import fsExtra from "fs-extra";

import { transErrors } from "./../../lang/vi";
import { app } from "./../config/app";

const LIMIT_CONVERSASTIONS_TAKEN = 10;
const LIMIT_MESSAGES_TAKEN = 30;
/**
 * get all conversations
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param {string} currentUserId 
 */
const getAllConversationItems = (currentUserId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let contacts = await ContactModel.getContacts(currentUserId, LIMIT_CONVERSASTIONS_TAKEN);

            let userConversationsPromise = contacts.map(async (contact) => {
                if (contact.contactId == currentUserId) {
                    let getUserContact = await UserModel.getNormalUserDataById(contact.userId);
                    getUserContact.updatedAt = contact.updatedAt;
                    return getUserContact;
                }
                else {
                    let getUserContact = await UserModel.getNormalUserDataById(contact.contactId);
                    getUserContact.updatedAt = contact.updatedAt;
                    return getUserContact;
                }
            });
            let userConversations = await Promise.all(userConversationsPromise);

            let groupConversations = await ChatGroupModel.getChatGroups(currentUserId, LIMIT_CONVERSASTIONS_TAKEN);

            let allConversations = userConversations.concat(groupConversations);

            allConversations = _.sortBy(allConversations, (item) => {
                return -item.updatedAt;
            });

            //get message to apply in screen chat
            let allConversationsWithMessagesPromise = allConversations.map(async (conversation) => {
                conversation = conversation.toObject(); //Convert sang object vì nó đang là dữ liệu kiểu mảng
                if (conversation.members) {
                    let getMessages = await MessageModel.model.getMessagesInGroup(conversation._id, LIMIT_MESSAGES_TAKEN);

                    conversation.messages = _.reverse(getMessages);
                } else {
                    let getMessages = await MessageModel.model.getMessagesInPersonal(currentUserId, conversation._id, LIMIT_MESSAGES_TAKEN);

                    conversation.messages = _.reverse(getMessages);
                }

                return conversation;
            })
            let allConversationsWithMessages = await Promise.all(allConversationsWithMessagesPromise);
            //sort by updatedAt desending
            allConversationsWithMessages = _.sortBy(allConversationsWithMessages, (item) => {
                return -item.updatedAt;
            });


            resolve({
                allConversationsWithMessages: allConversationsWithMessages
            });
        } catch (error) {
            reject(error);
        }
    })
};
/**
 * Add new message text and emoji
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param {object} sender currentUser
 * @param {string} receivedId an user or an group
 * @param {string} messageVal 
 * @param {boolean} isChatGroup 
 */
const addNewTextEmoji = (sender, receivedId, messageVal, isChatGroup) => {
    return new Promise(async (resolve, reject) => {
        if (isChatGroup) {
            let getChatGroupReceiver = await ChatGroupModel.getChatGroupById(receivedId);
            if (!getChatGroupReceiver) {
                return reject(transErrors.conversation_not_found);
            };
            let receiver = {
                id: getChatGroupReceiver._id,
                name: getChatGroupReceiver.name,
                avatar: app.general_avatar_group_chat
            };
            let newMessageItem = {
                senderId: sender.id,
                receiverId: receiver.id,
                conversationType: MessageModel.conversationTypes.GROUP,
                messageType: MessageModel.messageTypes.TEXT,
                sender: sender,
                receiver: receiver,
                text: messageVal,
                createdAt: Date.now()
            };

            // create new message
            let newMessage = await MessageModel.model.createNew(newMessageItem);
            await ChatGroupModel.updateWhenHasNewMessage(getChatGroupReceiver._id, getChatGroupReceiver.messageAmount + 1);

            resolve(newMessage);
        } else {
            let getUserReceiver = await UserModel.getNormalUserDataById(receivedId);
            if (!getUserReceiver) {
                return reject(transErrors.conversation_not_found);
            };
            let receiver = {
                id: getUserReceiver._id,
                name: getUserReceiver.name,
                avatar: getUserReceiver.avatar
            };
            let newMessageItem = {
                senderId: sender.id,
                receiverId: receiver.id,
                conversationType: MessageModel.conversationTypes.PERSONAL,
                messageType: MessageModel.messageTypes.TEXT,
                sender: sender,
                receiver: receiver,
                text: messageVal,
                createdAt: Date.now()
            };

            // create new message
            let newMessage = await MessageModel.model.createNew(newMessageItem);
            //update contact
            await ContactModel.updateWhenHasNewMessage(sender.id, getUserReceiver._id)
            resolve(newMessage);
        };
    });
};

/**
 * Add new message image
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param {object} sender currentUser
 * @param {string} receivedId an user or an group
 * @param {file} messageVal 
 * @param {boolean} isChatGroup 
 */
const addNewImage = (sender, receivedId, messageVal, isChatGroup) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (isChatGroup) {
                let getChatGroupReceiver = await ChatGroupModel.getChatGroupById(receivedId);
                if (!getChatGroupReceiver) {
                    return reject(transErrors.conversation_not_found);
                };
                let receiver = {
                    id: getChatGroupReceiver._id,
                    name: getChatGroupReceiver.name,
                    avatar: app.general_avatar_group_chat
                };

                let imageBuffer = await fsExtra.readFile(messageVal.path);
                let imageContentType = messageVal.mimetype;
                let imageName = messageVal.originalname;

                let newMessageItem = {
                    senderId: sender.id,
                    receiverId: receiver.id,
                    conversationType: MessageModel.conversationTypes.GROUP,
                    messageType: MessageModel.messageTypes.IMAGE,
                    sender: sender,
                    receiver: receiver,
                    file: { data: imageBuffer, contentType: imageContentType, fileName: imageName },
                    createdAt: Date.now()
                };

                // create new message
                let newMessage = await MessageModel.model.createNew(newMessageItem);
                await ChatGroupModel.updateWhenHasNewMessage(getChatGroupReceiver._id, getChatGroupReceiver.messageAmount + 1);

                resolve(newMessage);
            } else {
                let getUserReceiver = await UserModel.getNormalUserDataById(receivedId);
                if (!getUserReceiver) {
                    return reject(transErrors.conversation_not_found);
                };
                let receiver = {
                    id: getUserReceiver._id,
                    name: getUserReceiver.name,
                    avatar: getUserReceiver.avatar
                };
                let imageBuffer = await fsExtra.readFile(messageVal.path);
                let imageContentType = messageVal.mimetype;
                let imageName = messageVal.originalname;

                let newMessageItem = {
                    senderId: sender.id,
                    receiverId: receiver.id,
                    conversationType: MessageModel.conversationTypes.PERSONAL,
                    messageType: MessageModel.messageTypes.IMAGE,
                    sender: sender,
                    receiver: receiver,
                    file: { data: imageBuffer, contentType: imageContentType, fileName: imageName },
                    createdAt: Date.now()
                };

                // create new message
                let newMessage = await MessageModel.model.createNew(newMessageItem);
                //update contact
                await ContactModel.updateWhenHasNewMessage(sender.id, getUserReceiver._id)
                resolve(newMessage);
            };
        } catch (error) {
            reject(error);
        }

    });
};

/**
 * Add new message attachment
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param {object} sender currentUser
 * @param {string} receivedId an user or an group
 * @param {file} messageVal 
 * @param {boolean} isChatGroup 
 */
const addNewAttachment = (sender, receivedId, messageVal, isChatGroup) => {
    return new Promise(async (resolve, reject) => {
        try {
            if (isChatGroup) {
                let getChatGroupReceiver = await ChatGroupModel.getChatGroupById(receivedId);
                if (!getChatGroupReceiver) {
                    return reject(transErrors.conversation_not_found);
                };
                let receiver = {
                    id: getChatGroupReceiver._id,
                    name: getChatGroupReceiver.name,
                    avatar: app.general_avatar_group_chat
                };

                let attachmentBuffer = await fsExtra.readFile(messageVal.path);
                let attachmentContentType = messageVal.mimetype;
                let attachmentName = messageVal.originalname;

                let newMessageItem = {
                    senderId: sender.id,
                    receiverId: receiver.id,
                    conversationType: MessageModel.conversationTypes.GROUP,
                    messageType: MessageModel.messageTypes.FILE,
                    sender: sender,
                    receiver: receiver,
                    file: { data: attachmentBuffer, contentType: attachmentContentType, fileName: attachmentName },
                    createdAt: Date.now()
                };

                // create new message
                let newMessage = await MessageModel.model.createNew(newMessageItem);
                await ChatGroupModel.updateWhenHasNewMessage(getChatGroupReceiver._id, getChatGroupReceiver.messageAmount + 1);

                resolve(newMessage);
            } else {
                let getUserReceiver = await UserModel.getNormalUserDataById(receivedId);
                if (!getUserReceiver) {
                    return reject(transErrors.conversation_not_found);
                };
                let receiver = {
                    id: getUserReceiver._id,
                    name: getUserReceiver.name,
                    avatar: getUserReceiver.avatar
                };
                let attachmentBuffer = await fsExtra.readFile(messageVal.path);
                let attachmentContentType = messageVal.mimetype;
                let attachmentName = messageVal.originalname;

                let newMessageItem = {
                    senderId: sender.id,
                    receiverId: receiver.id,
                    conversationType: MessageModel.conversationTypes.PERSONAL,
                    messageType: MessageModel.messageTypes.FILE,
                    sender: sender,
                    receiver: receiver,
                    file: { data: attachmentBuffer, contentType: attachmentContentType, fileName: attachmentName },
                    createdAt: Date.now()
                };

                // create new message
                let newMessage = await MessageModel.model.createNew(newMessageItem);
                //update contact
                await ContactModel.updateWhenHasNewMessage(sender.id, getUserReceiver._id)
                resolve(newMessage);
            };
        } catch (error) {
            reject(error);
        }

    });
};

/**
 * read more personal and group chat
 * @param {string} currentUserId 
 * @param {number} skipPersonal 
 * @param {number} skipGroup 
 */
const readMoreAllChat = (currentUserId, skipPersonal, skipGroup) => {
    return new Promise(async (resolve, reject) => {
        try {
            let contacts = await ContactModel.readMoreContacts(currentUserId, skipPersonal, LIMIT_CONVERSASTIONS_TAKEN);

            let userConversationsPromise = contacts.map(async (contact) => {
                if (contact.contactId == currentUserId) {
                    let getUserContact = await UserModel.getNormalUserDataById(contact.userId);
                    getUserContact.updatedAt = contact.updatedAt;
                    return getUserContact;
                }
                else {
                    let getUserContact = await UserModel.getNormalUserDataById(contact.contactId);
                    getUserContact.updatedAt = contact.updatedAt;
                    return getUserContact;
                }
            });
            let userConversations = await Promise.all(userConversationsPromise);

            let groupConversations = await ChatGroupModel.readMoreChatGroups(currentUserId, skipGroup, LIMIT_CONVERSASTIONS_TAKEN);

            let allConversations = userConversations.concat(groupConversations);

            allConversations = _.sortBy(allConversations, (item) => {
                return -item.updatedAt;
            });

            //get message to apply in screen chat
            let allConversationsWithMessagesPromise = allConversations.map(async (conversation) => {
                conversation = conversation.toObject(); //Convert sang object vì nó đang là dữ liệu kiểu mảng
                if (conversation.members) {
                    let getMessages = await MessageModel.model.getMessagesInGroup(conversation._id, LIMIT_MESSAGES_TAKEN);

                    conversation.messages = _.reverse(getMessages);
                } else {
                    let getMessages = await MessageModel.model.getMessagesInPersonal(currentUserId, conversation._id, LIMIT_MESSAGES_TAKEN);

                    conversation.messages = _.reverse(getMessages);
                }

                return conversation;
            })
            let allConversationsWithMessages = await Promise.all(allConversationsWithMessagesPromise);
            //sort by updatedAt desending
            allConversationsWithMessages = _.sortBy(allConversationsWithMessages, (item) => {
                return -item.updatedAt;
            });


            resolve(allConversationsWithMessages);
        } catch (error) {
            reject(error);
        }
    })
};

/**
 * read more group chat
 * @param {string} currentUserId 
 * @param {number} skipGroup 
 */
const readMoreGroupChat = (currentUserId, skipGroup) => {
    return new Promise(async (resolve, reject) => {
        try {

            let groupConversations = await ChatGroupModel.readMoreChatGroups(currentUserId, skipGroup, LIMIT_CONVERSASTIONS_TAKEN);

            groupConversations = _.sortBy(groupConversations, (item) => {
                return -item.updatedAt;
            });

            //get message to apply in screen chat
            let allConversationsWithMessagesPromise = groupConversations.map(async (conversation) => {
                conversation = conversation.toObject(); //Convert sang object vì nó đang là dữ liệu kiểu mảng
                if (conversation.members) {
                    let getMessages = await MessageModel.model.getMessagesInGroup(conversation._id, LIMIT_MESSAGES_TAKEN);

                    conversation.messages = _.reverse(getMessages);
                }

                return conversation;
            })
            let allConversationsWithMessages = await Promise.all(allConversationsWithMessagesPromise);
            //sort by updatedAt desending
            allConversationsWithMessages = _.sortBy(allConversationsWithMessages, (item) => {
                return -item.updatedAt;
            });


            resolve(allConversationsWithMessages);
        } catch (error) {
            reject(error);
        }
    })
};

/**
 * read more personal and group chat
 * @param {string} currentUserId 
 * @param {number} skipPersonal 
 */
const readMoreUserChat = (currentUserId, skipPersonal) => {
    return new Promise(async (resolve, reject) => {
        try {
            let contacts = await ContactModel.readMoreContacts(currentUserId, skipPersonal, LIMIT_CONVERSASTIONS_TAKEN);

            let userConversationsPromise = contacts.map(async (contact) => {
                if (contact.contactId == currentUserId) {
                    let getUserContact = await UserModel.getNormalUserDataById(contact.userId);
                    getUserContact.updatedAt = contact.updatedAt;
                    return getUserContact;
                }
                else {
                    let getUserContact = await UserModel.getNormalUserDataById(contact.contactId);
                    getUserContact.updatedAt = contact.updatedAt;
                    return getUserContact;
                }
            });
            let userConversations = await Promise.all(userConversationsPromise);

            userConversations = _.sortBy(userConversations, (item) => {
                return -item.updatedAt;
            });

            //get message to apply in screen chat
            let allConversationsWithMessagesPromise = userConversations.map(async (conversation) => {
                conversation = conversation.toObject(); //Convert sang object vì nó đang là dữ liệu kiểu mảng

                let getMessages = await MessageModel.model.getMessagesInPersonal(currentUserId, conversation._id, LIMIT_MESSAGES_TAKEN);

                conversation.messages = _.reverse(getMessages);

                return conversation;
            })
            let allConversationsWithMessages = await Promise.all(allConversationsWithMessagesPromise);
            //sort by updatedAt desending
            allConversationsWithMessages = _.sortBy(allConversationsWithMessages, (item) => {
                return -item.updatedAt;
            });


            resolve(allConversationsWithMessages);
        } catch (error) {
            reject(error);
        }
    })
};

/**
 * read more messages
 * @param {string} currentUserId 
 * @param {number} skipMessage 
 * @param {string} targetId 
 * @param {boolean} chatInGroup 
 */
const readMore = (currentUserId, skipMessage, targetId, chatInGroup) => {
    return new Promise(async (resolve, reject) => {
        try {
            //Message in group
            if (chatInGroup) {
                let getMessages = await MessageModel.model.readMoreMessagesInGroup(targetId, skipMessage, LIMIT_MESSAGES_TAKEN);

                getMessages = _.reverse(getMessages);
                return resolve(getMessages);
            }

            //Message in personal
            let getMessages = await MessageModel.model.readMoreMessagesInPersonal(currentUserId, targetId, skipMessage, LIMIT_MESSAGES_TAKEN);
            getMessages = _.reverse(getMessages);
            return resolve(getMessages);
        } catch (error) {
            reject(error);
        }
    })
};

module.exports = {
    getAllConversationItems,
    addNewTextEmoji,
    addNewImage,
    addNewAttachment,
    readMoreAllChat,
    readMoreGroupChat,
    readMoreUserChat,
    readMore
};
