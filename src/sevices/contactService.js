import ContactModel from "./../models/contactModel";
import UserModel from "./../models/userModel";
import NotificationModel from "./../models/notificationModel";

import _ from "lodash";

const LIMIT_NUMBER_TAKEN = 10;

const findUsersContact = (currentUserId, keyword) => {
    return new Promise(async (resolve, reject) => {
        let deprecatedUserIds = [currentUserId];
        let contactsByUser = await ContactModel.findAllByUser(currentUserId);
        contactsByUser.forEach((contact) => {
            deprecatedUserIds.push(contact.userId);
            deprecatedUserIds.push(contact.contactId);
        });

        deprecatedUserIds = _.uniqBy(deprecatedUserIds);
        let users = await UserModel.findAllForContact(deprecatedUserIds, keyword);
        resolve(users);
    });
};

const addNew = (currentUserId, contactId) => {
    return new Promise(async (resolve, reject) => {
        let contactExists = await ContactModel.checkExists(currentUserId, contactId);
        if (contactExists) {
            return reject(false);
        }

        //create contact
        let newContactItem = {
            userId: currentUserId,
            contactId: contactId
        }
        let newContact = await ContactModel.createNew(newContactItem);

        //Notification
        let notificationItem = {
            senderId: currentUserId,
            receiverId: contactId,
            type: NotificationModel.types.ADD_CONTACT,
        };
        await NotificationModel.model.createNew(notificationItem);
        resolve(newContact);
    });
};

const removeContact = (currentUserId, contactId) => {
    return new Promise(async (resolve, reject) => {
        let removeContact = await ContactModel.removeContact(currentUserId, contactId);
        if (removeContact.result.n === 0) {
            return reject(false);
        }
        resolve(true);
    });
}

const removeRequestContactSent = (currentUserId, contactId) => {
    return new Promise(async (resolve, reject) => {
        let removeReq = await ContactModel.removeRequestContactSent(currentUserId, contactId);
        if (removeReq.result.n === 0) {
            return reject(false);
        }
        //Remove notification 
        let notifTypeAddContact = NotificationModel.types.ADD_CONTACT;
        await NotificationModel.model.removeRequestContactNotification(currentUserId, contactId, notifTypeAddContact);
        resolve(true);
    });
};

const removeRequestContactReceived = (currentUserId, contactId) => {
    return new Promise(async (resolve, reject) => {
        let removeReq = await ContactModel.removeRequestContactReceived(currentUserId, contactId);
        if (removeReq.result.n === 0) {
            return reject(false);
        }
        //Remove notification 
        //Trên thực tế thì không nên xóa thông báo gửi lời kết bạn khi hủy lời gửi kết bạn
        // let notifTypeAddContact = NotificationModel.types.ADD_CONTACT;
        // await NotificationModel.model.removeRequestContactNotification(currentUserId, contactId, notifTypeAddContact);
        resolve(true);
    });
};

const approveRequestContactReceived = (currentUserId, contactId) => {
    return new Promise(async (resolve, reject) => {
        let approveReq = await ContactModel.approveRequestContactReceived(currentUserId, contactId);
        if (approveReq.nModified === 0) {
            return reject(false);
        }
        //create notification 
        let notificationItem = {
            senderId: currentUserId,
            receiverId: contactId,
            type: NotificationModel.types.APPROVE_CONTACT,
        };
        await NotificationModel.model.createNew(notificationItem);
        resolve(true);
    });
};

const getContacts = (currentUserId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let contacts = await ContactModel.getContacts(currentUserId, LIMIT_NUMBER_TAKEN);

            let users = contacts.map(async (contact) => {
                if (contact.contactId == currentUserId) {
                    return await UserModel.getNormalUserDataById(contact.userId);
                }
                else {
                    return await UserModel.getNormalUserDataById(contact.contactId);
                }
            });

            resolve(await Promise.all(users));
        } catch (error) {
            reject(error);
        }
    });
};

const getContactsSent = (currentUserId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let contacts = await ContactModel.getContactsSent(currentUserId, LIMIT_NUMBER_TAKEN);

            let users = contacts.map(async (contact) => {
                return await UserModel.getNormalUserDataById(contact.contactId);
            });

            resolve(await Promise.all(users));
        } catch (error) {
            reject(error);
        }
    });
};

const getContactsReceived = (currentUserId) => {
    return new Promise(async (resolve, reject) => {
        try {
            let contacts = await ContactModel.getContactsReceived(currentUserId, LIMIT_NUMBER_TAKEN);

            let users = contacts.map(async (contact) => {
                return await UserModel.getNormalUserDataById(contact.userId);
            });

            resolve(await Promise.all(users));
        } catch (error) {
            reject(error);
        }
    });
};

const countAllContacts = (currentUserId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const count = await ContactModel.countAllContacts(currentUserId);

            resolve(count);
        } catch (error) {
            reject(error);
        }
    });
};

const countAllContactsSent = (currentUserId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const count = await ContactModel.countAllContactsSent(currentUserId);

            resolve(count);
        } catch (error) {
            reject(error);
        }
    });
};

const countAllContactsReceived = (currentUserId) => {
    return new Promise(async (resolve, reject) => {
        try {
            const count = await ContactModel.countAllContactsReceived(currentUserId);

            resolve(count);
        } catch (error) {
            reject(error);
        }
    });
};

/**===============
 * Service of Read more contact
 * Create By : KamiTeam-Nguyễn Văn Phong
 */
const readMoreContacts = (currentUserId, skipNumberContacts) => {
    return new Promise(async (resolve, reject) => {
        try {
            let newContacts = await ContactModel.readMoreContacts(currentUserId, skipNumberContacts, LIMIT_NUMBER_TAKEN);
            let users = newContacts.map(async (contact) => {
                if (contact.contactId == currentUserId) {
                    return await UserModel.getNormalUserDataById(contact.userId);
                }
                else {
                    return await UserModel.getNormalUserDataById(contact.contactId);
                }
            });

            resolve(await Promise.all(users));
        } catch (error) {
            reject(error);
        }
    });
};

/**===============
 * Service of Read more contact sent
 * Create By : KamiTeam-Nguyễn Văn Phong
 */
const readMoreContactsSent = (currentUserId, skipNumberContacts) => {
    return new Promise(async (resolve, reject) => {
        try {
            let newContacts = await ContactModel.readMoreContactsSent(currentUserId, skipNumberContacts, LIMIT_NUMBER_TAKEN);
            let users = newContacts.map(async (contact) => {
                return await UserModel.getNormalUserDataById(contact.contactId);
            });

            resolve(await Promise.all(users));
        } catch (error) {
            reject(error);
        }
    });
};

/**===============
 * Service of Read more contact sent
 * Create By : KamiTeam-Nguyễn Văn Phong
 */
const readMoreContactsReceived = (currentUserId, skipNumberContacts) => {
    return new Promise(async (resolve, reject) => {
        try {
            let newContacts = await ContactModel.readMoreContactsReceived(currentUserId, skipNumberContacts, LIMIT_NUMBER_TAKEN);
            let users = newContacts.map(async (contact) => {
                return await UserModel.getNormalUserDataById(contact.userId);
            });

            resolve(await Promise.all(users));
        } catch (error) {
            reject(error);
        }
    });
};

const searchFriends = (currentUserId, keyword) => {
    return new Promise(async (resolve, reject) => {
        let friendIds = [];
        let friends = await ContactModel.getFriends(currentUserId);

        friends.forEach((item)=>{
            friendIds.push(item.userId);
            friendIds.push(item.contactId);
        });

        friendIds = _.uniqBy(friendIds);
        friendIds = friendIds.filter(userId => userId != currentUserId);

        let users = await UserModel.findAllToAddGroupChat(friendIds, keyword);

        resolve(users);
    });
};



module.exports = {
    findUsersContact,
    addNew,
    removeContact,
    removeRequestContactSent,
    removeRequestContactReceived,
    approveRequestContactReceived,
    getContacts,
    getContactsSent,
    getContactsReceived,
    countAllContactsReceived,
    countAllContactsSent,
    countAllContacts,
    readMoreContacts,
    readMoreContactsSent,
    readMoreContactsReceived,
    searchFriends
};
