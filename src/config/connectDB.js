import mongoose from 'mongoose';
import bluebird from 'bluebird';
const { MongoClient } = require('mongodb');
require('dotenv').config();
/**==================
 * Connect to MongoDB
 * Create by : KamiTeam - Nguyễn Văn Phong
 */
let connectDB = () => {
    mongoose.Promise = bluebird;
    //mongodb://localhost:27017/kachat
    //let URI = `${process.env.DB_CONNECTION}://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`;
    let URI = `mongodb://kachat:IXtj2fhYYrTXmx8Q@kachat-shard-00-00-fydq4.mongodb.net:27017,kachat-shard-00-01-fydq4.mongodb.net:27017,kachat-shard-00-02-fydq4.mongodb.net:27017/test?ssl=true&replicaSet=kachat-shard-0&authSource=admin&retryWrites=true&w=majority`

    return mongoose.connect(URI, {useUnifiedTopology: true, useNewUrlParser: true, useCreateIndex: true })
};

module.exports = connectDB;
