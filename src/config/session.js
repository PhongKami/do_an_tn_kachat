import session from "express-session";
import connectMongo from "connect-mongo";

let MongoStore = connectMongo(session);

/**===============
 * This variable is where save session, in this case is mongodb
 * Create by : KamiTeam - Nguyễn Văn Phong
 */
let sessionStore = new MongoStore({
    // url: `${process.env.DB_CONNECTION}://${process.env.DB_HOST}:${process.env.DB_PORT}/${process.env.DB_NAME}`,
    url: `mongodb://kachat:IXtj2fhYYrTXmx8Q@kachat-shard-00-00-fydq4.mongodb.net:27017,kachat-shard-00-01-fydq4.mongodb.net:27017,kachat-shard-00-02-fydq4.mongodb.net:27017/test?ssl=true&replicaSet=kachat-shard-0&authSource=admin&retryWrites=true&w=majority`,
    autoReconnect: true
    // autoRemove: "native" // Khi cookie hết hạn thì nó tự động xóa trên cơ sở sữ liệu
});

/**===============
 * Config session for app 
 * Create by : KamiTeam - Nguyễn Văn Phong
 */
let config = (app) => {
    app.use(session({
        key: process.env.SECTION_KEY,
        secret: process.env.SECTION_SECRET,
        store: sessionStore,
        resave: true,
        saveUninitialized: false,
        cookie: {
            maxAge: 1000 * 60 * 60 * 24 //86400000 seconds= 1 day
        }
    }));
};

module.exports = {
    config,
    sessionStore
};
