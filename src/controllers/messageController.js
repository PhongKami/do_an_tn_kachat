import { validationResult } from "express-validator/check";
import multer from "multer";
import fsExtra from "fs-extra";
import ejs from "ejs";
import { promisify } from "util";

import { app } from "./../config/app";
import { transErrors, transSuccess } from "./../../lang/vi";
import { message } from "./../sevices/index";
import { lastItemOfArray, convertTimestampToHumanTime, bufferToBase64 } from "./../helpers/clientHelper";

//Make ejs function renderfile available with async await
const renderFile = promisify(ejs.renderFile).bind(ejs);

//Handle text and emoji chat
const addNewTextEmoji = async (req, res) => {
    let errorArray = [];
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
        let err = Object.values(validationErrors.mapped());
        err.forEach(item => {
            errorArray.push(item.msg);
        });
        return res.status(500).send(errorArray);
    }

    try {
        let sender = {
            id: req.user._id,
            name: req.user.username,
            avatar: req.user.avatar
        };
        let receivedId = req.body.uid;
        let messageVal = req.body.messageVal;
        let isChatGroup = req.body.isChatGroup;

        let newMessage = await message.addNewTextEmoji(sender, receivedId, messageVal, isChatGroup);
        return res.status(200).send({ message: newMessage });
    } catch (error) {
        return res.status(500).send(error);
    }
};

//Handle image chat
const storageImageChat = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, app.image_message_directory);
    },
    filename: (req, file, callback) => {
        const math = app.image_message_type;
        if (math.indexOf(file.mimetype) === -1) {
            return callback(transErrors.image_message_type, null);
        }

        //Xử lí để lấy tên ảnh. Chứ mong muốn của mình là nó lưu luôn vào trong database chứ không phải lưu trên server nên không cần thiết dùng uuidv4 để mất thêm hiệu xuất
        //let imageName = `${Date.now()}-${uuidv4()}-${file.originalname}`;
        let imageName = `${file.originalname}`;
        callback(null, imageName);
    }
});

const imageMessageUploadFile = multer({
    storage: storageImageChat,
    limits: { fileSize: app.image_message_limit_size }
}).single("my-image-chat");

const addNewImage = async (req, res) => {
    imageMessageUploadFile(req, res, async (error) => {
        if (error) {
            if (error.message) {
                return res.status(500).send(transErrors.image_message_size);
            }
            return res.status(500).send(error);
        };

        try {
            let sender = {
                id: req.user._id,
                name: req.user.username,
                avatar: req.user.avatar
            };
            let receivedId = req.body.uid;
            let messageVal = req.file;
            let isChatGroup = req.body.isChatGroup;

            let newMessage = await message.addNewImage(sender, receivedId, messageVal, isChatGroup);

            //Remove image, because this image is save to mongodb 
            await fsExtra.remove(`${app.image_message_directory}/${newMessage.file.fileName}`);

            return res.status(200).send({ message: newMessage });
        } catch (error) {
            return res.status(500).send(error);
        }
    });

};

//Handle attachment chat
const storageAttachmentChat = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, app.attachment_message_directory);
    },
    filename: (req, file, callback) => {
        //Xử lí để lấy tên ảnh. Chứ mong muốn của mình là nó lưu luôn vào trong database chứ không phải lưu trên server nên không cần thiết dùng uuidv4 để mất thêm hiệu xuất
        //let imageName = `${Date.now()}-${uuidv4()}-${file.originalname}`;
        let attachmentName = `${file.originalname}`;
        callback(null, attachmentName);
    }
});

const attachmentMessageUploadFile = multer({
    storage: storageAttachmentChat,
    limits: { fileSize: app.attachment_message_limit_size }
}).single("my-attachment-chat");

const addNewAttachment = async (req, res) => {
    attachmentMessageUploadFile(req, res, async (error) => {
        if (error) {
            if (error.message) {
                return res.status(500).send(transErrors.attachment_message_size);
            }
            return res.status(500).send(error);
        };

        try {
            let sender = {
                id: req.user._id,
                name: req.user.username,
                avatar: req.user.avatar
            };
            let receivedId = req.body.uid;
            let messageVal = req.file;
            let isChatGroup = req.body.isChatGroup;

            let newMessage = await message.addNewAttachment(sender, receivedId, messageVal, isChatGroup);

            //Remove attachment, because this attachment is save to mongodb 
            await fsExtra.remove(`${app.attachment_message_directory}/${newMessage.file.fileName}`);

            return res.status(200).send({ message: newMessage });
        } catch (error) {
            return res.status(500).send(error);
        }
    });

};

const readMoreAllChat = async (req, res) => {
    try {
        //get skip number from query param
        let skipPersonal = +(req.query.skipPersonal);
        let skipGroup = +(req.query.skipGroup);
        //get more item 
        const newAllConversations = await message.readMoreAllChat(req.user._id, skipPersonal, skipGroup);

        let dataToRender = {
            newAllConversations: newAllConversations,
            lastItemOfArray: lastItemOfArray,
            convertTimestampToHumanTime: convertTimestampToHumanTime,
            bufferToBase64: bufferToBase64,
            user: req.user
        }
        const leftSideData = await renderFile("src/views/main/readMoreConversations/_leftSide.ejs", dataToRender);

        const rightSideData = await renderFile("src/views/main/readMoreConversations/_rightSide.ejs", dataToRender);
        const imageModalData = await renderFile("src/views/main/readMoreConversations/_imageModal.ejs", dataToRender);
        const attachmentModalData = await renderFile("src/views/main/readMoreConversations/_attachmentModal.ejs", dataToRender);

        return res.status(200).send({
            leftSideData,
            rightSideData,
            imageModalData,
            attachmentModalData
        });
    } catch (error) {
        return res.status(500).send(error);
    }
};
const readMoreGroupChat = async (req, res) => {
    try {
        //get skip number from query param
        let skipGroup = +(req.query.skipGroup);
        //get more item 
        const newAllConversations = await message.readMoreGroupChat(req.user._id, skipGroup);

        let dataToRender = {
            newAllConversations: newAllConversations,
            lastItemOfArray: lastItemOfArray,
            convertTimestampToHumanTime: convertTimestampToHumanTime,
            bufferToBase64: bufferToBase64,
            user: req.user
        }
        const leftSideData = await renderFile("src/views/main/readMoreConversations/_leftSide.ejs", dataToRender);

        const rightSideData = await renderFile("src/views/main/readMoreConversations/_rightSide.ejs", dataToRender);
        const imageModalData = await renderFile("src/views/main/readMoreConversations/_imageModal.ejs", dataToRender);
        const attachmentModalData = await renderFile("src/views/main/readMoreConversations/_attachmentModal.ejs", dataToRender);

        return res.status(200).send({
            leftSideData,
            rightSideData,
            imageModalData,
            attachmentModalData
        });
    } catch (error) {
        return res.status(500).send(error);
    }
};
const readMoreUserChat = async (req, res) => {
    try {
        //get skip number from query param
        let skipPersonal = +(req.query.skipPersonal);
        //get more item 
        const newAllConversations = await message.readMoreUserChat(req.user._id, skipPersonal);

        let dataToRender = {
            newAllConversations: newAllConversations,
            lastItemOfArray: lastItemOfArray,
            convertTimestampToHumanTime: convertTimestampToHumanTime,
            bufferToBase64: bufferToBase64,
            user: req.user
        }
        const leftSideData = await renderFile("src/views/main/readMoreConversations/_leftSide.ejs", dataToRender);

        const rightSideData = await renderFile("src/views/main/readMoreConversations/_rightSide.ejs", dataToRender);
        const imageModalData = await renderFile("src/views/main/readMoreConversations/_imageModal.ejs", dataToRender);
        const attachmentModalData = await renderFile("src/views/main/readMoreConversations/_attachmentModal.ejs", dataToRender);

        return res.status(200).send({
            leftSideData,
            rightSideData,
            imageModalData,
            attachmentModalData
        });
    } catch (error) {
        return res.status(500).send(error);
    }
};

const readMore = async (req, res) => {
    try {
        //get skip number from query param
        let skipMessage = +(req.query.skipMessage);
        let targetId = req.query.targetId;
        let chatInGroup = (req.query.chatInGroup === true);

        //get more item 
        const newMessages = await message.readMore(req.user._id, skipMessage, targetId, chatInGroup);

        let dataToRender = {
            newMessages: newMessages,
            bufferToBase64: bufferToBase64,
            user: req.user
        }

        const rightSideData = await renderFile("src/views/main/readMoreMessages/_rightSide.ejs", dataToRender);
        const imageModalData = await renderFile("src/views/main/readMoreMessages/_imageModal.ejs", dataToRender);
        const attachmentModalData = await renderFile("src/views/main/readMoreMessages/_attachmentModal.ejs", dataToRender);

        return res.status(200).send({
            rightSideData,
            imageModalData,
            attachmentModalData
        });
    } catch (error) {
        return res.status(500).send(error);
    }
};

module.exports = {
    addNewTextEmoji,
    addNewImage,
    addNewAttachment,
    readMoreAllChat,
    readMoreGroupChat,
    readMoreUserChat,
    readMore
};
