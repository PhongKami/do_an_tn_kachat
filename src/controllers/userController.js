import multer from "multer";
import { app } from "./../config/app";
import { transErrors, transSuccess } from "./../../lang/vi";
import uuidv4 from "uuid/v4";
import { user } from "./../sevices/index";
import fsExtra from "fs-extra";
import { validationResult } from "express-validator/check";

/**=========
 * Cấu hình nơi lưu trữ
 */
const storageAvatar = multer.diskStorage({
    destination: (req, file, callback) => {
        callback(null, app.avatar_directory);
    },
    filename: (req, file, callback) => {
        const math = app.avatar_type;
        if (math.indexOf(file.mimetype) === -1) {
            return callback(transErrors.avatar_type, null);
        }

        let avatarName = `${Date.now()}-${uuidv4()}-${file.originalname}`;
        callback(null, avatarName);
    }
});

const avatarUploadFile = multer({
    storage: storageAvatar,
    limits: { fileSize: app.avatar_limit_size }
}).single("avatar");

const updateAvatar = (req, res) => {
    avatarUploadFile(req, res, async (err) => {
        if (err) {
            if (err.message) {
                return res.status(500).send(transErrors.avatar_size);
            }
            return res.status(500).send(err);
        };
        try {
            let updateUserItem = {
                avatar: req.file.filename,
                updateAt: Date.now()
            };

            //Update user 
            const userUpdate = await user.updateUser(req.user._id, updateUserItem);

            //Remove old user avatar
            //Không nên xóa avatar cũ của người dùng vì trong bảng message cần được sử dụng
            //await fsExtra.remove(`${app.avatar_directory}/${userUpdate.avatar}`);

            let result = {
                message: transSuccess.user_info_updated,
                imageSrc: `/images/users/${req.file.filename}`
            };
            return res.status(200).send(result);
        } catch (error) {
            return res.status(500).send(error);
        }
    });
};

const updateInfo = async (req, res) => {
    let errorArray = [];
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
        let err = Object.values(validationErrors.mapped());
        err.forEach(item => {
            errorArray.push(item.msg);
        });
        return res.status(500).send(errorArray);
    }
    try {
        let updateUserItem = req.body;
        await user.updateUser(req.user._id, updateUserItem);

        let result = {
            message: transSuccess.user_info_updated
        };
        return res.status(200).send(result);
    } catch (error) {
        return res.status(500).send(error);
    }
}

const updatePassword = async (req, res) => {
    let errorArray = [];
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
        let err = Object.values(validationErrors.mapped());
        err.forEach(item => {
            errorArray.push(item.msg);
        });
        return res.status(500).send(errorArray);
    }

    try {
        const updateUserItem = req.body;
        await user.updatePassword(req.user._id, updateUserItem);

        let result = {
            message: transSuccess.user_password_updated
        }
        return res.status(200).send(result);
    } catch (error) {
        return res.status(500).send(error);

    }
}

module.exports = {
    updateAvatar,
    updateInfo,
    updatePassword
};
