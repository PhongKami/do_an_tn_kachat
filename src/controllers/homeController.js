import { notification, contact, message } from "./../sevices/index";
import { bufferToBase64, lastItemOfArray, convertTimestampToHumanTime } from "./../helpers/clientHelper";
import request from "request";

const getICETurnServer = () => {
    return new Promise(async (resolve, reject) => {
        //Mở comment thì mới gọi điện ngoài mạng được

        //Node Get ICE STUN and TURN list
        let o = {
            format: "urls"
        };

        let bodyString = JSON.stringify(o);

        let options = {
            url: "https://global.xirsys.net/_turn/Kachat",
            // host: "global.xirsys.net", Dùng biến url ở trên rồi
            // path: "/_turn/Kachat",
            method: "PUT",
            headers: {
                "Authorization": "Basic " + Buffer.from("PhongKami:478f36cc-5d3c-11ea-bad7-0242ac110004").toString("base64"),
                "Content-Type": "application/json",
                "Content-Length": bodyString.length
            }
        };

        //Call a request to get ICE STUN and TURN list
        request(options, (error, response, body) => {
            if(error){
                console.log("Error when get ICE list "+ error);
                return reject(error);
            }
            let bodyJson = JSON.parse(body);
            resolve(bodyJson.v.iceServers);
        });

        // resolve([]);
    });
}


const getHome = async (req, res) => {
    //Only 10 items one time
    let notifications = await notification.getNotifications(req.user._id);
    //Get amount notifications unread
    let countNotifUnread = await notification.countNotifUnread(req.user._id);

    //Get contacts 10 items one time
    let contacts = await contact.getContacts(req.user._id);
    //Get contacts send
    let contactsSent = await contact.getContactsSent(req.user._id);
    //Get contact received
    let contactsReceived = await contact.getContactsReceived(req.user._id);

    //Count contacts
    let countAllContacts = await contact.countAllContacts(req.user._id);
    let countAllContactsSent = await contact.countAllContactsSent(req.user._id);
    let countAllContactsReceived = await contact.countAllContactsReceived(req.user._id);

    let getAllConversationItems = await message.getAllConversationItems(req.user._id);
    //all message with conversation, max 30 messages
    let allConversationsWithMessages = getAllConversationItems.allConversationsWithMessages;

    //get ICE list TURN server from xirsys turn server
    let iceServerList = await getICETurnServer();

    return res.render("main/home/home", {
        errors: req.flash("errors"),
        success: req.flash("success"),
        user: req.user,
        notifications: notifications,
        countNotifUnread: countNotifUnread,
        contacts: contacts,
        contactsSent: contactsSent,
        contactsReceived: contactsReceived,
        countAllContactsReceived: countAllContactsReceived,
        countAllContactsSent: countAllContactsSent,
        countAllContacts: countAllContacts,
        allConversationsWithMessages: allConversationsWithMessages,
        bufferToBase64: bufferToBase64,
        lastItemOfArray: lastItemOfArray,
        convertTimestampToHumanTime: convertTimestampToHumanTime,
        iceServerList: JSON.stringify(iceServerList)
    });
};

module.exports = {
    getHome,
    getICETurnServer
};
