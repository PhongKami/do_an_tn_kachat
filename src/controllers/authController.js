import { validationResult } from "express-validator/check";
import { auth } from "./../sevices/index";
import { transSuccess } from "../../lang/vi";

//Kiểm tra, điều hướng form đăng nhập
const getLoginRegister = (req, res) => {
    return res.render("auth/master", {
        errors: req.flash("errors"),
        success: req.flash("success")
    });
};

// đăng xuất tài khoản
const getLogout = (req, res) => {
    req.logout(); //remove sesion passport
    req.flash("success", transSuccess.logout_success);
    return res.redirect("/login-register");
}

// đăng kí tài khoản
const postRegister = async (req, res) => {
    let errorArray = [];
    let successAray = [];
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
        let err = Object.values(validationErrors.mapped());
        err.forEach(item => {
            errorArray.push(item.msg);
        });
        req.flash("errors", errorArray);
        return res.redirect("/login-register");
    }

    try {
        let createUserSuccess = await auth.register(req.body.email, req.body.gender, req.body.password, req.protocol, req.get("host"));

        successAray.push(createUserSuccess);
        req.flash("success", successAray);
        return res.redirect("/login-register");
    }
    catch (error) {
        errorArray.push(error);
        req.flash("errors", errorArray);
        return res.redirect("/login-register");
    }

};

//Kiểm tra Tài khoản
const verifyAccount = async (req, res) => {
    let errorArray = [];
    let successAray = [];

    try {
        let verifySuccess = await auth.verifyAccount(req.params.token);
        successAray.push(verifySuccess);

        req.flash("success", successAray);
        return res.redirect("/login-register");
    }
    catch (error) {
        errorArray.push(error);
        req.flash("errors", errorArray);
        return res.redirect("/login-register");
    }
};

//kiểm tra xem đã đăng nhập hay chưa
const checkLoggedIn = (req, res, next) => {
    if (!req.isAuthenticated()) {
        return res.redirect("/login-register");
    }
    next();
}

//Kiểm tra đã đăng xuất hay chưa
const checkLoggedOut = (req, res, next) => {
    if (req.isAuthenticated()) {
        return res.redirect("/");
    }
    next();
}

module.exports = {
    getLoginRegister,
    getLogout,
    postRegister,
    verifyAccount,
    getLogout,
    checkLoggedIn,
    checkLoggedOut
};
