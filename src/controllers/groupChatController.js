import { validationResult } from "express-validator/check";
import { groupChat } from "./../sevices/index";

const addNewGroup = async (req, res) => {
    let errorArray = [];
    const validationErrors = validationResult(req);

    if (!validationErrors.isEmpty()) {
        let err = Object.values(validationErrors.mapped());
        err.forEach(item => {
            errorArray.push(item.msg);
        });
        //Logging
        return res.status(500).send(errorArray);
    }

    try {
        let currentUserId = req.user._id;
        let arrayMemberIds = req.body.arrayIds;
        let groupChatName = req.body.groupChatName;

        let newGroupChat = await groupChat.addNewGroup(currentUserId, arrayMemberIds, groupChatName);
        return res.status(200).send({ groupChat: newGroupChat });

    } catch (error) {
        return res.status(500).send(error);
    }
};

module.exports = {
    addNewGroup
}
