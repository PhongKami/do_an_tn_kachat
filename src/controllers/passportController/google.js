import passport from "passport";
import passportGoogle from "passport-google-oauth2";
import UserModel from "./../../models/userModel";
import ChatGroupModel from "./../../models/chatGroupModel";

import { transErrors, transSuccess } from "./../../../lang/vi";
require('dotenv').config();
//const GoogleStrategy = require('passport-google-auth').Strategy;
const GoogleStrategy = passportGoogle.Strategy;
const ggAppId = process.env.GG_APP_ID;
const ggAppSecret = process.env.GG_APP_SECRET;
const ggAppCallbackUrl = process.env.GG_CALLBACK_URL;

/**=============
 * Valid user account type: Google
 * Create by : KamiTeam - Nguyễn Văn Phong
 */
const initPassportGoogle = () => {
    passport.use(new GoogleStrategy({
        clientID: ggAppId,
        clientSecret: ggAppSecret,
        callbackURL: ggAppCallbackUrl,
        passReqToCallback: true
    }, async (req, accessToken, refreshToken, profile, done) => {
        try {
            let user = await UserModel.findByGoogleUid(profile.id);
            if (user) {
                return done(null, user, req.flash("success", transSuccess.login_success(user.username)));
            }
            console.log(profile);
            let newUserItem = {
                username: profile.displayName,
                gender: profile.gender,
                local: {
                    isActive: true
                },
                google: {
                    uid: profile.id,
                    token: accessToken,
                    email: profile.emails[0].value
                }
            };
            const newUser = await UserModel.createNew(newUserItem);
            return done(null, newUser, req.flash("success", transSuccess.login_success(newUser.username)));
        }
        catch (error) {
            console.log(error);
            return done(null, false, req.flash("errors", transErrors.server_error));
        }
    }));
    //save userID to session
    passport.serializeUser((user, done) => {
        done(null, user._id);
    });

    passport.deserializeUser(async (id, done) => {
        try {
            let user = await UserModel.findUserByIdForSessionToUser(id);
            let getChatGroupIds = await ChatGroupModel.getChatGroupIdsByUser(user._id);
            user = user.toObject();
            user.chatGroupIds = getChatGroupIds;
            return done(null, user);

        } catch (error) {
            return done(error, null);
        }
    });
};

module.exports = initPassportGoogle;
