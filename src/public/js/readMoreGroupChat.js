$(document).ready(function () {
    $("#link-read-more-group-chat").bind('click', function () {
        let skipGroup = $("#group-chat").find("li.group-chat").length;

        $("#link-read-more-group-chat").css("display", "none");
        $(".read-more-group-chat-loader").css("display", "inline-block");
        // setTimeout(() => { //Nên bỏ đi khi mạng chậm
        $.get(`/message/read-more-group-chat?skipGroup=${skipGroup}`, function (data) {
            if (data.leftSideData.trim() === "") {
                alertify.notify("Bạn không còn cuộc trò chuyện nào để xem nữa cả!", "error", 7);
                $("#link-read-more-group-chat").css("display", "inline-block");
                $(".read-more-group-chat-loader").css("display", "none");
                return false;
            }

            //Step 1: handle left side
            $("#group-chat").find("ul").append(data.leftSideData);

            //Step 2: handle scroll left
            resizeNiceScrollLeft();
            nineScrollLeft();

            //Step 3: handle right side
            $("#screen-chat").append(data.rightSideData);

            //Step 4: Call function screenChat
            changeScreenChat();

            //Step 5: Convert emoji
            convertEmoji();

            //Step 6: Handle image Modal
            $("body").append(data.imageModalData);

            //Step 7: Call function gridPhoto
            gridPhotos(5);

            //Step 8: handle attachment
            $("body").append(data.attachmentModalData);

            //Step 9: update online
            socket.emit("check-status");



            $("#link-read-more-group-chat").css("display", "inline-block");
            $(".read-more-group-chat-loader").css("display", "none");
        });
        // }, 1000);

    });
});
