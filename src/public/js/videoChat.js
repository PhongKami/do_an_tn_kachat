function videoChat(divId) {
    $(`#video-chat-${divId}`).unbind("click").on("click", function () {
        let targetId = $(this).data("chat");
        let callerName = $("#navbar-username").text();

        let dataToEmit = {
            listenerId: targetId,
            callerName: callerName
        };

        //Step 1: of Caller
        socket.emit("caller-check-listener-online-or-not", dataToEmit);
    });
};

//Function play video stream
function playVideoStream(videoTagId, stream) {
    let video = document.getElementById(videoTagId);
    video.srcObject = stream;
    video.onloadeddata = function () {
        video.play();
    };
};

//Function close video stream
function closeVideoStream(stream) {
    return stream.getTracks().forEach(track => track.stop());
};

$(document).ready(function () {
    // Step 2 of Caller
    socket.on("server-send-listener-is-offline", function () {
        alertify.notify("Người dùng hiện không online!", "error", 7);
    });

    let iceServerList = $("#ice-server-list").val();

    let getPeerId = "";
    const peer = new Peer({
        key: "peerjs",
        host: "kamiteam-peerjs-server.herokuapp.com",
        secure: true,
        port: 443,
        config: {"iceServers":JSON.parse(iceServerList)},
        debug: 3
    });

    peer.on("open", function (peerId) {
        getPeerId = peerId;

    });

    //Step 3 of Listener
    socket.on("server-request-peer-id-of-listener", function (response) {
        let listenerName = $("#navbar-username").text();
        let dataToEmit = {
            callerId: response.callerId,
            listenerId: response.listenerId,
            callerName: response.callerName,
            listenerName: listenerName,
            listenerPeerId: getPeerId
        };
        //Step 4 of Listemer
        socket.emit("listener-emit-peer-id-to-server", dataToEmit);
    });

    let timerInterval;
    //Step 5 of Caller
    socket.on("server-send-peer-id-of-listener-to-caller", function (response) {
        let dataToEmit = {
            callerId: response.callerId,
            listenerId: response.listenerId,
            callerName: response.callerName,
            listenerName: response.listenerName,
            listenerPeerId: response.listenerPeerId
        };

        //Step 6 off Caller
        socket.emit("caller-request-call-to-server", dataToEmit);

        Swal.fire({
            title: `Đang gọi cho &nbsp;&nbsp;<span style="color: #2ECC71">${response.listenerName}</span> &nbsp;&nbsp; <i class="fa fa-volume-control-phone"></i>`,
            html: `
                Thời gian : <strong style="color: #d43f3a"></strong> giây.<br><br>
                <button id="btn-cancel-call" class="btn btn-danger">Hủy cuộc gọi</button>
                `,
            backdrop: "rgb(85, 85, 85, 0.4)",
            width: "52rem",
            allowOutsideClick: false,
            timer: 30000, //30 seconds
            onBeforeOpen: () => {
                $("#btn-cancel-call").unbind("click").on("click", function () {
                    Swal.close();
                    clearInterval(timerInterval);

                    //Step 7 of caller
                    socket.emit("caller-cancel-request-call-to-server", dataToEmit);
                });

                if (Swal.getContent().querySelector !== null) {
                    Swal.showLoading();
                    timerInterval = setInterval(() => {
                        Swal.getContent().querySelector("strong").textContent = Math.ceil(Swal.getTimerLeft() / 1000);
                    }, 1000);
                }
            },
            onOpen: () => {
                //Step 12 of caller
                socket.on("server-send-reject-call-to-caller", function (response) {
                    Swal.close();
                    clearInterval(timerInterval);

                    Swal.fire({
                        type: "info",
                        title: `<span style="color: #2ECC71">${response.listenerName}</span> &nbsp;&nbsp; hiện tại không thể nghe máy!`,
                        backdrop: "rgb(85, 85, 85, 0.4)",
                        width: "52rem",
                        allowOutsideClick: false,
                        confirmButtonColor: "#2ECC71",
                        confirmButtonText: "Xác nhận",
                    });
                });

            },
            onClose: () => {
                clearInterval(timerInterval);
            }
        }).then((result) => {
            return false;
        });
    });

    //Step 8 of Lítener
    socket.on("server-send-request-call-to-listener", function (response) {
        let dataToEmit = {
            callerId: response.callerId,
            listenerId: response.listenerId,
            callerName: response.callerName,
            listenerName: response.listenerName,
            listenerPeerId: response.listenerPeerId
        };

        Swal.fire({
            title: `<span style="color: #2ECC71">${response.callerName}</span> &nbsp;&nbsp; muốn trò chuyện video với bạn &nbsp;&nbsp;<i class="fa fa-volume-control-phone"></i>`,
            html: `
                Thời gian : <strong style="color: #d43f3a"></strong> giây.<br><br>
                <button id="btn-reject-call" class="btn btn-danger">Từ chối</button>
                <button id="btn-accept-call" class="btn btn-success">Đồng ý</button>
                `,
            backdrop: "rgb(85, 85, 85, 0.4)",
            width: "52rem",
            allowOutsideClick: false,
            timer: 30000, //30 seconds
            onBeforeOpen: () => {
                $("#btn-reject-call").unbind("click").on("click", function () {
                    Swal.close();
                    clearInterval(timerInterval);

                    //Step 10 of listener
                    socket.emit("listener-reject-request-call-to-server", dataToEmit);
                });
                //Step 11 of listener
                $("#btn-accept-call").unbind("click").on("click", function () {
                    Swal.close();
                    clearInterval(timerInterval);

                    socket.emit("listener-accept-request-call-to-server", dataToEmit);
                });

                if (Swal.getContent().querySelector !== null) {
                    Swal.showLoading();
                    timerInterval = setInterval(() => {
                        Swal.getContent().querySelector("strong").textContent = Math.ceil(Swal.getTimerLeft() / 1000);
                    }, 1000);
                }
            },
            onOpen: () => {
                //Step 9 of listener
                socket.on("server-send-cancel-request-call-to-listener", function (response) {
                    Swal.close();
                    clearInterval(timerInterval);

                    Swal.fire({
                        type: "info",
                        title: `<span style="color: #2ECC71">${response.callerName}</span>&nbsp;&nbsp; Đã gọi nhỡ cho bạn `,
                        backdrop: "rgb(85, 85, 85, 0.4)",
                        width: "52rem",
                        allowOutsideClick: false,
                        confirmButtonColor: "#2ECC71",
                        confirmButtonText: "Xác nhận",
                    });
                });

            },
            onClose: () => {
                clearInterval(timerInterval);
            }
        }).then((result) => {
            return false;
        });
    });
    //Step 13 of caller
    socket.on("server-send-accept-call-to-caller", function (response) {
        Swal.close();
        clearInterval(timerInterval);

        let getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia).bind(navigator);

        getUserMedia({ video: true, audio: true }, function (stream) {
            //Show modal streaming
            $("#streamModal").modal("show");

            //Play my stream in local (of caller);
            playVideoStream("local-stream", stream);
            //call to listener
            let call = peer.call(response.listenerPeerId, stream);

            //listen & play tream of listener
            call.on("stream", function (remoteStream) {

                //Play stream of listener in local
                playVideoStream("remote-stream", remoteStream);
            });

            //Close modal: remove stream 
            $("#streamModal").on("hidden.bs.modal", function () {
                closeVideoStream(stream);

                Swal.fire({
                    type: "info",
                    title: `Đã kết thúc cuộc gọi với &nbsp;&nbsp;<span style="color: #2ECC71">${response.listenerName}</span> `,
                    backdrop: "rgb(85, 85, 85, 0.4)",
                    width: "52rem",
                    allowOutsideClick: false,
                    confirmButtonColor: "#2ECC71",
                    confirmButtonText: "Xác nhận",
                });
            });
        }, function (err) {
            //Bắt lỗi tắt thiết bị
            if(err.toString() === "NotAllowedError: Permission denied") {
                alertify.notify("Xin lỗi bạn đã tắt quyền truy cập vào thiết bị nghe gọi trên trình duyệt. Vui lòng mở lại trong phần cài đặt của trình duyệt", "error", 7);
            };
            //Bát lỗi thiết bị không hỗ trợ camera
            if(err.toString() === "NotAllowedError: Requested device not found") {
                alertify.notify("Xin lỗi, chúng tôi không tìm thấy thiết bị nghe gọi trong máy tinh của bạn! =))", "error", 7);
            };
        });
    });

    //Step 14 of listener
    socket.on("server-send-accept-call-to-listener", function (response) {
        Swal.close();
        clearInterval(timerInterval);

        let getUserMedia = (navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia).bind(navigator);

        peer.on("call", function (call) {
            getUserMedia({ video: true, audio: true }, function (stream) {
                //Show modal streaming
                $("#streamModal").modal("show");

                //Play my stream in local (of listener);
                playVideoStream("local-stream", stream);

                call.answer(stream); // Answer the call with an A/V stream.

                call.on("stream", function (remoteStream) {
                    //Play stream of caller in local 
                    playVideoStream("remote-stream", remoteStream);
                });
                //Close modal: remove stream 
                $("#streamModal").on("hidden.bs.modal", function () {
                    closeVideoStream(stream);

                    Swal.fire({
                        type: "info",
                        title: `Đã kết thúc cuộc gọi với &nbsp;&nbsp;<span style="color: #2ECC71">${response.callerName}</span> `,
                        backdrop: "rgb(85, 85, 85, 0.4)",
                        width: "52rem",
                        allowOutsideClick: false,
                        confirmButtonColor: "#2ECC71",
                        confirmButtonText: "Xác nhận",
                    });
                });
            }, function (err) {
                if(err.toString() === "NotAllowedError: Permission denied") {
                    alertify.notify("Xin lỗi bạn đã tắt quyền truy cập vào thiết bị nghe gọi trên trình duyệt. Vui lòng mở lại trong phần cài đặt của trình duyệt", "error", 7);
                };
                if(err.toString() === "NotAllowedError: Requested device not found") {
                    alertify.notify("Xin lỗi, chúng tôi không tìm thấy thiết bị nghe gọi trong máy tinh của bạn! =))", "error", 7);
                };
            });
        });
    });
});
