function callFindUsers(element) {
    if (element.which === 13 || element.type === "click") {
        let keyword = $("#input-find-users-contact").val();
        const regexKeyword= new RegExp(/^[\s0-9a-zA-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]+$/);
        
        if (!keyword.length) {
            alertify.notify("Chưa nhập nội dung tìm kiếm", "error", 7);
            return false;
        };

        if (!regexKeyword.test(keyword)) {
            alertify.notify("Lỗi từ khóa tìm kiếm! Chỉ cho phép sử dụng kí tự chữ, số và khoảng cách! ", "error", 7);
            return false;
        };

        //Ajax
        $.get(`/contact/find-users/${keyword}`, function (data) {
            $("#find-user ul"). html(data);
            addContact(); //js/addContact.js
            removeRequestContactSent(); //js/removeRequestContactSent.js
        });
    };
};

$(document).ready(function () {
    //When user press Enter
    $("#input-find-users-contact").bind("keypress", callFindUsers);
    //When user click search
    $("#btn-find-users-contact").bind("click", callFindUsers);
});
