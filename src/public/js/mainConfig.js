/**
 * Created by KamiTeam-Nguyễn Văn Phong.
 */

const socket = io();
/**
 * Thanh cuộn trái - ScrollLeft
 */
function nineScrollLeft() {
  $('.left').niceScroll({
    smoothscroll: true,
    horizrailenabled: false,
    cursorcolor: '#ECECEC',
    cursorwidth: '7px',
    scrollspeed: 50
  });
}
/**
 * Tùy chỉnh kích thước cho thanh cuộn trái - resize scroll left
 */
function resizeNiceScrollLeft() {
  $('.left').getNiceScroll().resize();
}
/**
 * Thanh cuộn phải - scroll right
 * @param {string} divId 
 */
function nineScrollRight(divId) {
  $(`.right .chat[data-chat=${divId}]`).niceScroll({
    smoothscroll: true,
    horizrailenabled: false,
    cursorcolor: '#ECECEC',
    cursorwidth: '7px',
    scrollspeed: 50
  });
  $(`.right .chat[data-chat=${divId}]`).scrollTop($(`.right .chat[data-chat=${divId}]`)[0].scrollHeight);
}
/**
 * Emoji đính kèm -attach emoji
 * @param {string} divId 
 */
function enableEmojioneArea(divId) {
  $(`#write-chat-${divId}`).emojioneArea({
    standalone: false,
    pickerPosition: 'top',
    filtersPosition: 'bottom',
    tones: false,
    autocomplete: false,
    inline: true,
    hidePickerOnBlur: true,
    search: false,
    shortnames: false,
    events: {
      keyup: function (editor, event) {
        //Gán giá trị thay đổi vào thẻ input đã bị ẩn
        $(`#write-chat-${divId}`).val(this.getText());
      },
      click: function () {
        //Bật lắng nghe DOM cho việc chat text và emoji
        textAndEmojiChat(divId);
        //Bật chức năng người dùng đang gõ tin nhắn
        typingOn(divId);
      },
      blur: function () {
        //Tắt chức năng người dùng đang gõ tin nhắn
        typingOff(divId);
      }
    },
  });
  $('.icon-chat').bind('click', function (event) {
    event.preventDefault();
    $('.emojionearea-button').click();
    $('.emojionearea-editor').focus();
  });
}
/**
 *  ẩn Load trang - display spin load
 */
function spinLoaded() {
  $('.master-loader').css('display', 'none');
}
/**
 * hiện load trang - hide spin load
 */
function spinLoading() {
  $('.master-loader').css('display', 'block');
}

/**
 * Sử dụng ajax load trang
 */
function ajaxLoading() {
  $(document)
    .ajaxStart(function () {
      spinLoading();
    })
    .ajaxStop(function () {
      spinLoaded();
    });
}

/**
 * Hiển thị màn hình quản lý liên lạc - show display modal contacts
 */
function showModalContacts() {
  $('#show-modal-contacts').click(function () {
    $(this).find('.noti_contact_counter').fadeOut('slow');
  });
}
/**
 * cấu hình thông báo - config notification
 */
function configNotification() {
  $('#noti_Button').click(function () {
    $('#notifications').fadeToggle('fast', 'linear');
    $('.noti_counter').fadeOut('slow');
    return false;
  });
  $(".main-content").click(function () {
    $('#notifications').fadeOut('fast', 'linear');
  });
}

/**
 * trình bày hình ảnh - grid photo.
 * @param {number} layoutNumber 
 */
function gridPhotos(layoutNumber) {
  $(".show-images").unbind("click").on("click", function () {
    let href = $(this).attr("href");
    let modalImagesId = href.replace("#", "");

    let originDataImage = $(`#${modalImagesId}`).find("div.modal-body").html();

    let countRows = Math.ceil($(`#${modalImagesId}`).find("div.all-images>img").length / layoutNumber);
    let layoutStr = new Array(countRows).fill(layoutNumber).join("");

    $(`#${modalImagesId}`).find("div.all-images").photosetGrid({
      highresLinks: true,
      rel: "withhearts-gallery",
      gutter: "2px",
      layout: layoutStr,
      onComplete: function () {
        $(`#${modalImagesId}`).find(".all-images").css({
          "visibility": "visible"
        });
        $(`#${modalImagesId}`).find(".all-images a").colorbox({
          photo: true,
          scalePhotos: true,
          maxHeight: "90%",
          maxWidth: "90%"
        });
      }
    });
    //Bắt sự kiện đóng modal https://stackoverflow.com/a/12319710
    $(`#${modalImagesId}`).on('hidden.bs.modal', function () {
      $(this).find("div.modal-body").html(originDataImage);
    });

  });
}

//Để hiển thị cái tạo group chat
// function showButtonGroupChat() {
//   $('#select-type-chat').bind('change', function () {
//     if ($(this).val() === 'group-chat') {
//       $('.create-group-chat').show();
//       // Do something...
//     } else {
//       $('.create-group-chat').hide();
//     }
//   });
// }

// function addFriendsToGroup() {
//   $('ul#group-chat-friends').find('div.add-user').bind('click', function () {
//     let uid = $(this).data('uid');
//     $(this).remove();
//     let html = $('ul#group-chat-friends').find('div[data-uid=' + uid + ']').html();

//     let promise = new Promise(function (resolve, reject) {
//       $('ul#friends-added').append(html);
//       $('#groupChatModal .list-user-added').show();
//       resolve(true);
//     });
//     promise.then(function (success) {
//       $('ul#group-chat-friends').find('div[data-uid=' + uid + ']').remove();
//     });
//   });
// }

// function cancelCreateGroup() {
//   $('#cancel-group-chat').bind('click', function () {
//     $('#groupChatModal .list-user-added').hide();
//     if ($('ul#friends-added>li').length) {
//       $('ul#friends-added>li').each(function (index) {
//         $(this).remove();
//       });
//     }
//   });
// }

//Notify ở màn hình master
function flashMasterNotify() {
  let notify = $(".master-success-message").text();
  if (notify.length) {
    alertify.notify(notify, "success", 7);
  }
}

function changeTypeChat() {
  $("#select-type-chat").bind("change", function () {
    let optionSelected = $("option:selected", this);
    optionSelected.tab("show");

    if ($(this).val() === "user-chat") {
      $(".create-group-chat").hide();
    }
    else {
      $(".create-group-chat").show();
    }
  });
};
/**
 * Thay đổi khung chat - change screen chat
 */
function changeScreenChat() {
  $(".room-chat").unbind("click").on("click", function () {
    $(".person").removeClass("active");
    let divId = $(this).find("li").data("chat");

    $(`.person[data-chat=${divId}]`).addClass("active");
    $(this).tab("show");

    //Cấu hình thanh cuộn bên box chat rightSide.js mỗi khi click chuột vào cuộc trò chuyện cụ thể 
    nineScrollRight(divId);

    // Bật emoji, tham số truyền vào là id của box nhập nội dung tin nhắn
    enableEmojioneArea(divId);

    //Bật lắng nghe cho việc chát tin nhắn hình ảnh
    imageChat(divId);

    //Bật lắng nghe cho việc chát tin nhắn tệp tin đính kèm
    attachmentChat(divId);

    //Bật lắng nghe DOM cho việc gọi video
    videoChat(divId);
  });
};
/**
 * Chuyển đổi emoji khi hiển thị - convert emoji
 */
function convertEmoji() {
  $(".convert-emoji").each(function () {
    var original = $(this).html();
    // use .shortnameToImage if only converting shortnames (for slightly better performance)
    var converted = emojione.toImage(original);
    $(this).html(converted);
  });
};

/**
 * Chuyển đổi từ dạng bufer sang Base64 - change from buffer to base 64
 * @param {string} buffer 
 */
function bufferToBase64(buffer) {
  return btoa(
    new Uint8Array(buffer).reduce((data, byte) => data + String.fromCharCode(byte), ""));
};

/**
 * Phóng to ảnh khi click vào - zoom image chat
 */
function zoomImageChat() {
  $(".show-image-chat").unbind("click").on("click", function () {
    console.log("Phongad")
    $("#img-chat-modal").css("display", "block");
    $("#img-chat-modal-content").attr("src", $(this)[0].src);

    $("#img-chat-modal").on("click", function () {
      $(this).css("display", "none");
    });
  });
}

/**
 * Mở màn hình chat khi chọn bạn bè trong danh bạ
 */
function userTalk() {
  $(".user-talk").unbind("click").on("click", function () {
    const dataChat = $(this).data("uid");
    console.log(dataChat);
    $("ul.people").find(`a[href="#uid_${dataChat}"]`).click();
    $(this).closest("div.modal").modal("hide");
  });
}

/**
 * Mở tìm kiếm bạn bè khi người dùng mới tạo tài khoản
 */
function notYetConversations() {
  if (!$("ul.people").find("a").length) {
    Swal.fire({
      title: "Bạn chưa có bạn bè? Hày tìm kiếm bạn bè để trò chuyện! =)",
      type: "info",
      showCancelButton: false,
      confirmButtonColor: "#2ECC71",
      confirmButtonText: "Xác nhận",
    }).then(result => {
      $("#contactsModal").modal("show");
    });
  }
};

$(document).ready(function () {
  // Hide số thông báo trên đầu icon mở modal contact
  showModalContacts();

  // Bật tắt popup notification
  configNotification();

  // Cấu hình thanh cuộn
  nineScrollLeft();
  //nineScrollRight();

  // Icon loading khi chạy ajax
  ajaxLoading();

  // Hiển thị button mở modal tạo nhóm trò chuyện
  //showButtonGroupChat();

  // Hiển thị hình ảnh grid slide trong modal tất cả ảnh, tham số truyền vào là số ảnh được hiển thị trên 1 hàng.
  // Tham số chỉ được phép trong khoảng từ 1 đến 5
  gridPhotos(5);

  // // Thêm người dùng vào danh sách liệt kê trước khi tạo nhóm trò chuyện
  // addFriendsToGroup();

  // // Action hủy việc tạo nhóm trò chuyện
  // cancelCreateGroup();

  //Flash message ở màn hình master
  flashMasterNotify();

  //Thay đổi cuộc trò chuyện all-group-user
  changeTypeChat();

  //Thay đổi màn hình chat
  changeScreenChat();

  //convert các unicode thành hình cảnh cảm xúc
  convertEmoji();

  //Mở modal tìm kiếm bạn bè khi người dùng mới tạo tài khoảns
  notYetConversations();

  userTalk();

  zoomImageChat();

  //Click vào phần tử đầu tiên của cuộc trò chuyện khi reload trang 
  if ($("ul.people").find("a").length) {
    $("ul.people").find("a")[0].click();
  }

  $("#video-chat-group").bind("click", function () {
    alertify.notify("Không khả dụng tính năng này với nhóm trò chuyện. Vui lòng thử lại với trò chuyện cá nhân", "error", 7);
  })

});
