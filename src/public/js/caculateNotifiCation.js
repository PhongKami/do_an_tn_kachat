//Giẩm số lượng khi hủy lời mời kết bạn
function decreaseNumberNotifiCation(className, number) {
    let currentValue = +$(`.${className}`).text();
    currentValue -= number;

    if (currentValue === 0) {
        $(`.${className}`).css("display", "none").html("");
    }
    else {
        $(`.${className}`).css("display", "block").html(currentValue);
    }
};
//Tăng số lượng khi hủy lời mời kết bạn
function increaseNumberNotifiCation(className, number) {
    let currentValue = +$(`.${className}`).text();
    currentValue += number;

    if (currentValue === 0) {
        $(`.${className}`).css("display", "none").html("");
    }
    else {
        $(`.${className}`).css("display", "block").html(currentValue);
    }
};
