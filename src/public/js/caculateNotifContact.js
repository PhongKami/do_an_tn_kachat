//Giẩm số lượng khi hủy lời mời kết bạn
function decreaseNumberNotifContact(className) {
    let currentValue = +$(`.${className}`).find("em").text();
    currentValue -= 1;

    if (currentValue === 0) {
        $(`.${className}`).html("");
    }
    else {
        $(`.${className}`).html(`(<em> ${currentValue} </em>)`);
    }
};
//Tăng số lượng khi hủy lời mời kết bạn
function increaseNumberNotifContact(className) {
    let currentValue = +$(`.${className}`).find("em").text();
    currentValue+=1;

    if (currentValue===0) {
        $(`.${className}`).html("");
    }
    else{
        $(`.${className}`).html(`(<em> ${currentValue} </em>)`);
    }
};
