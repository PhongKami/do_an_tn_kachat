
function removeRequestContactReceived() {
    $(".user-remove-request-contact-received").unbind("click").on("click", function () {
        let targetId = $(this).data("uid");
        $.ajax({
            url: "/contact/remove-request-contact-received",
            type: "delete",
            data: { uid: targetId },
            success: function (data) {
                if (data.success) {
                    //Chức năng này chưa muốn làm =)) Nguyên nhân ở phía bên socket.on bên dưới
                    // $(".noti_content").find(`div[data-uid=${user.id}]`).remove(); //popup notif
                    // $("ul.list-notifications").find(`li>div[data-uid=${user.id}]`).parent().remove(); //modal notif
                    decreaseNumberNotifiCation("noti_contact_counter", 1);// js/caculateNotification.js

                    decreaseNumberNotifContact("count-request-contact-received");// js/caculateNotifContact.js

                    //Xóa ở modal tab yêu cầu kết bạn
                    $("#request-contact-received").find(`li[data-uid=${targetId}]`).remove();
                    //Xử lý realtime ...
                    socket.emit("remove-request-contact-received", { contactId: targetId });
                }
            }
        })
    });
}

socket.on("response-remove-request-contact-received", function (user) {
    $("#find-user").find(`div.user-remove-request-contact-sent[data-uid= ${user.id}]`).hide();
    $("#find-user").find(`div.user-add-new-contact[data-uid= ${user.id}]`).css("display", "inline-block");

    //Hai dòng dưới để xóa thông báo nếu có người gửi yêu cầu kết bạn mà mình lại hủy yêu cầu kết bạn đó đi.
    //Nhưng trên thực tế thì nên giữ lại cái thông báo đó để cho người dùng biết người đó đã gửi thông báo cho mình 1 lần rôi.
    // $(".noti_content").find(`div[data-uid=${user.id}]`).remove(); //popup notif
    // $("ul.list-notifications").find(`li>div[data-uid=${user.id}]`).parent().remove(); //modal notif
    //Xóa thông báo
    //decreaseNumberNotifiCation("noti_counter", 1);// js/caculateNotification.js

    //Xóa ở modal tab đang chờ xác nhận
    $("#request-contact-sent").find(`li[data-uid=${user.id}]`).remove();

    decreaseNumberNotifContact("count-request-contact-sent");

    decreaseNumberNotifiCation("noti_contact_counter", 1);// js/caculateNotification.js
});

$(document).ready(function () {
    removeRequestContactReceived();
});
