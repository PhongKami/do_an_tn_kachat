import { pushSocketIdToAray, emitNotifyToAray, removeSocketIdFromAray } from "./../../helpers/socketHelper";

/**
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param  io from socket.io lib
 */
let chatVideo = (io) => {
    let clients = {};

    io.on("connection", (socket) => {
        //Push socket id to array
        clients = pushSocketIdToAray(clients, socket.request.user._id, socket.id);
        socket.request.user.chatGroupIds.forEach(group => {
            clients = pushSocketIdToAray(clients, group._id, socket.id);
        });
        //When has new group caht
        socket.on("new-group-created", (data) => {
            clients = pushSocketIdToAray(clients, data.groupChat._id, socket.id);
        });

        socket.on("member-received-group-chat", (data)=>{
            clients = pushSocketIdToAray(clients, data.groupChatId, socket.id);
        });

        //When you chat with your friend
        socket.on("caller-check-listener-online-or-not", (data) => {
            if (clients[data.listenerId]) {
                //online
                let response = {
                    callerId: socket.request.user._id,
                    listenerId: data.listenerId,
                    callerName: data.callerName
                };

                emitNotifyToAray(clients, data.listenerId, io, "server-request-peer-id-of-listener", response);
            }
            else {
                //offline
                socket.emit("server-send-listener-is-offline");
            }
        });

        socket.on("listener-emit-peer-id-to-server", (data) => {
            let response = {
                callerId: data.callerId,
                listenerId: data.listenerId,
                callerName: data.callerName,
                listenerName: data.listenerName,
                listenerPeerId: data.listenerPeerId
            };
            if (clients[data.callerId]) {
                emitNotifyToAray(clients, data.callerId, io, "server-send-peer-id-of-listener-to-caller", response)
            }
        });
        //sau bước 6 server lắng nghe yếu cầu gọi của người gọi  server bắn sang cho người nhận
        socket.on("caller-request-call-to-server", (data) => {
            let response = {
                callerId: data.callerId,
                listenerId: data.listenerId,
                callerName: data.callerName,
                listenerName: data.listenerName,
                listenerPeerId: data.listenerPeerId
            };
            if (clients[data.listenerId]) {
                emitNotifyToAray(clients, data.listenerId, io, "server-send-request-call-to-listener", response)
            }
        });
        //Nếu người gọi hủy cuộc gọi thì nó sẽ thông báo cho người bên kia
        socket.on("caller-cancel-request-call-to-server", (data) => {
            let response = {
                callerId: data.callerId,
                listenerId: data.listenerId,
                callerName: data.callerName,
                listenerName: data.listenerName,
                listenerPeerId: data.listenerPeerId
            };
            if (clients[data.listenerId]) {
                emitNotifyToAray(clients, data.listenerId, io, "server-send-cancel-request-call-to-listener", response)
            }
        });
        //Server lắng nghe sự kiện từ chối cuộc gọi
        socket.on("listener-reject-request-call-to-server", (data) => {
            let response = {
                callerId: data.callerId,
                listenerId: data.listenerId,
                callerName: data.callerName,
                listenerName: data.listenerName,
                listenerPeerId: data.listenerPeerId
            };
            if (clients[data.callerId]) {
                emitNotifyToAray(clients, data.callerId, io, "server-send-reject-call-to-caller", response)
            }
        });
        //Server lắng nghe sự kiện đồng ý nhận  cuộc gọi
        socket.on("listener-accept-request-call-to-server", (data) => {
            let response = {
                callerId: data.callerId,
                listenerId: data.listenerId,
                callerName: data.callerName,
                listenerName: data.listenerName,
                listenerPeerId: data.listenerPeerId
            };
            if (clients[data.callerId]) {
                emitNotifyToAray(clients, data.callerId, io, "server-send-accept-call-to-caller", response)
            };
            if (clients[data.listenerId]) {
                emitNotifyToAray(clients, data.listenerId, io, "server-send-accept-call-to-listener", response)
            }
        });

        socket.on("disconnect", () => {
            //Remove socketId when socket disconnect
            clients = removeSocketIdFromAray(clients, socket.request.user._id, socket);
            socket.request.user.chatGroupIds.forEach(group => {
                clients = removeSocketIdFromAray(clients, group._id, socket);
            });
        });
    });
};

module.exports = chatVideo;
