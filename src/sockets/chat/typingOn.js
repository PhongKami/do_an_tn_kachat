import { pushSocketIdToAray, emitNotifyToAray, removeSocketIdFromAray } from "./../../helpers/socketHelper";

/**
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param  io from socket.io lib
 */
let typingOn = (io) => {
    let clients = {};

    io.on("connection", (socket) => {
        //Push socket id to array
        clients = pushSocketIdToAray(clients, socket.request.user._id, socket.id);
        socket.request.user.chatGroupIds.forEach(group => {
            clients = pushSocketIdToAray(clients, group._id, socket.id);
        });
        //When has new group caht
        socket.on("new-group-created", (data) => {
            clients = pushSocketIdToAray(clients, data.groupChat._id, socket.id);
        });

        socket.on("member-received-group-chat", (data)=>{
            clients = pushSocketIdToAray(clients, data.groupChatId, socket.id);
        });

        //When you chat with your friend
        socket.on("user-is-typing", (data) => {
            if (data.groupId) {
                let response = {
                    currentGroupId: data.groupId,
                    currentUserId: socket.request.user._id
                };

                //emit chat
                if (clients[data.groupId]) {
                    emitNotifyToAray(clients, data.groupId, io, "response-user-is-typing", response);
                }
            };

            if (data.contactId) {
                let response = {
                    currentUserId: socket.request.user._id
                };

                //emit chat
                if (clients[data.contactId]) {
                    emitNotifyToAray(clients, data.contactId, io, "response-user-is-typing", response);
                }
            }
        });

        socket.on("disconnect", () => {
            //Remove socketId when socket disconnect
            clients = removeSocketIdFromAray(clients, socket.request.user._id, socket);
            socket.request.user.chatGroupIds.forEach(group => {
                clients = removeSocketIdFromAray(clients, group._id, socket);
            });
        });
    });
};

module.exports = typingOn;
