import { pushSocketIdToAray, emitNotifyToAray, removeSocketIdFromAray } from "./../../helpers/socketHelper";

/**
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param  io from socket.io lib
 */
let newGroupChat = (io) => {
    let clients = {};

    io.on("connection", (socket) => {
        //Push socket id to array
        clients = pushSocketIdToAray(clients, socket.request.user._id, socket.id);
        socket.request.user.chatGroupIds.forEach(group => {
            clients = pushSocketIdToAray(clients, group._id, socket.id);
        });
        socket.on("new-group-created", (data) => {
            clients = pushSocketIdToAray(clients, data.groupChat._id, socket.id);
            
            let response = {
                groupChat : data.groupChat
            };

            data.groupChat.members.forEach(member =>{
                if (clients[member.userId] && member.userId != socket.request.user._id) {
                    emitNotifyToAray(clients, member.userId, io, "response-new-group-created", response);
                }
            });

        });

        socket.on("member-received-group-chat", (data)=>{
            clients = pushSocketIdToAray(clients, data.groupChatId, socket.id);

        });

        socket.on("disconnect", () => {
            //Remove socketId when socket disconnect
            clients = removeSocketIdFromAray(clients, socket.request.user._id, socket);
            socket.request.user.chatGroupIds.forEach(group => {
                clients = removeSocketIdFromAray(clients, group._id, socket);
            });
        });
    });
};

module.exports = newGroupChat;
