import { pushSocketIdToAray, emitNotifyToAray, removeSocketIdFromAray } from "./../../helpers/socketHelper";

/**
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param  io from socket.io lib
 */
let approveRequestContactReceived = (io) => {
    let clients = {};

    io.on("connection", (socket) => {
        //Push socket id to array
        clients = pushSocketIdToAray(clients, socket.request.user._id, socket.id);
        socket.on("approve-request-contact-received", (data) => {
            let currentUser = {
                id: socket.request.user._id,
                username: socket.request.user.username,
                avatar: socket.request.user.avatar,
                address: (socket.request.user.address !== null) ? socket.request.user.address : ""
            };

            //emit notification
            if (clients[data.contactId]) {
                emitNotifyToAray(clients, data.contactId, io, "response-approve-request-contact-received", currentUser);
            }
        });

        socket.on("disconnect", () => {
            //Remove socketId when socket disconnect
            clients = removeSocketIdFromAray(clients, socket.request.user._id, socket);
        });
    });
};

module.exports = approveRequestContactReceived;
