import { pushSocketIdToAray, emitNotifyToAray, removeSocketIdFromAray } from "./../../helpers/socketHelper";
/**
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param  io from socket.io lib
 */const removeContact = (io) => {
    let clients = {};

    io.on("connection", (socket) => {
        //Push socket id to array
        clients = pushSocketIdToAray(clients, socket.request.user._id, socket.id);

        socket.on("remove-contact", (data) => {
            let currentUser = {
                id: socket.request.user._id
            };

            //emit notification
            if (clients[data.contactId]) {
                emitNotifyToAray(clients, data.contactId, io, "response-remove-contact", currentUser);
            }
        });

        socket.on("disconnect", () => {
            //Remove socketId when socket disconnect
            clients = removeSocketIdFromAray(clients, socket.request.user._id, socket);
        });
    });
};

module.exports = removeContact;
