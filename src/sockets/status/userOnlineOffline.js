import { pushSocketIdToAray, emitNotifyToAray, removeSocketIdFromAray } from "./../../helpers/socketHelper";

/**
 * Create By : KamiTeam-Nguyễn Văn Phong
 * @param  io from socket.io lib
 */
let userOnlineOffline = (io) => {
    let clients = {};

    io.on("connection", (socket) => {
        //Push socket id to array
        clients = pushSocketIdToAray(clients, socket.request.user._id, socket.id);
        socket.request.user.chatGroupIds.forEach(group => {
            clients = pushSocketIdToAray(clients, group._id, socket.id);
        });

        //When has new group caht
        socket.on("new-group-created", (data) => {
            clients = pushSocketIdToAray(clients, data.groupChat._id, socket.id);
        });

        socket.on("member-received-group-chat", (data) => {
            clients = pushSocketIdToAray(clients, data.groupChatId, socket.id);
        });

        //When you chat with your friend
        socket.on("check-status", () => {
            let listUserOnine = Object.keys(clients);
            //Step 1 : Emit to user after login or F5 web page
            socket.emit("server-send-list-users-online", listUserOnine);

            //Step 2 : Emit to all another users when has user online
            socket.broadcast.emit("server-send-when-new-user-online", socket.request.user._id);
        });


        socket.on("disconnect", () => {
            //Remove socketId when socket disconnect
            clients = removeSocketIdFromAray(clients, socket.request.user._id, socket);
            socket.request.user.chatGroupIds.forEach(group => {
                clients = removeSocketIdFromAray(clients, group._id, socket);
            });

            //Step 3 : Emit to all another users when has user offline
            socket.broadcast.emit("server-send-when-new-user-offline", socket.request.user._id);
        });
    });
};

module.exports = userOnlineOffline;
