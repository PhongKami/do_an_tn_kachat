import express from 'express';
import { home, auth, user, contact, notification, message, groupChat } from "../controllers/index";
const router = express.Router();
import { authValid, userValid, contactValid, messageValid, groupChatValid } from "./../validation/index";
import initPassportLocal from "./../controllers/passportController/local";
import initPassportFacebook from "./../controllers/passportController/facebook";
import initPassportGoogle from "./../controllers/passportController/google";
import passport from 'passport';

/**============================
 * Init all passport
 * Create By : KamiTeam - Nguyễn Văn Phong
 */
initPassportLocal();
initPassportFacebook();
initPassportGoogle();

/**===========================
 * Init all router
 * @param app from exactly express module
 * Create by : KamiTeam - Nguyễn Văn Phong
 */
const initRoutes = (app) => {
    //Route login-out
    router.get("/", auth.checkLoggedIn, home.getHome);
    router.get("/login-register", auth.checkLoggedOut, auth.getLoginRegister);
    router.post("/register", auth.checkLoggedOut, authValid.register, auth.postRegister);
    router.get("/verify/:token", auth.checkLoggedOut, auth.verifyAccount);
    router.post("/login", auth.checkLoggedOut, passport.authenticate("local", {
        successRedirect: "/",
        failureRedirect: "/login-register",
        successFlash: true,
        failureFlash: true
    }))
    router.get("/logout", auth.checkLoggedIn, auth.getLogout);
    //Route of facebook
    router.get("/auth/facebook", auth.checkLoggedOut, passport.authenticate("facebook", { scope: ["email"] }));
    router.get("/auth/facebook/callback", auth.checkLoggedOut, passport.authenticate("facebook", {
        successRedirect: "/",
        failureRedirect: "/login-register"
    }));
    //Route of google
    router.get("/auth/google", auth.checkLoggedOut, passport.authenticate("google", { scope: ["openid", "email", "profile"] }));
    router.get("/auth/google/callback", auth.checkLoggedOut, passport.authenticate("google", {
        successRedirect: "/",
        failureRedirect: "/login-register"
    }));
    //Route update info
    router.put("/user/update-avatar", auth.checkLoggedIn, user.updateAvatar);
    router.put("/user/update-info", auth.checkLoggedIn, userValid.updateInfo, user.updateInfo);
    router.put("/user/update-password", auth.checkLoggedIn, userValid.updatePassword, user.updatePassword);

    //Route of contact
    router.get("/contact/find-users/:keyword", auth.checkLoggedIn, contactValid.findUsersContact, contact.findUsersContact);
    router.post("/contact/add-new", auth.checkLoggedIn, contact.addNew);
    router.delete("/contact/remove-contact", auth.checkLoggedIn, contact.removeContact);
    router.delete("/contact/remove-request-contact-sent", auth.checkLoggedIn, contact.removeRequestContactSent);
    router.delete("/contact/remove-request-contact-received", auth.checkLoggedIn, contact.removeRequestContactReceived);
    router.put("/contact/approve-request-contact-received", auth.checkLoggedIn, contact.approveRequestContactReceived);
    router.get("/contact/read-more-contacts", auth.checkLoggedIn, contact.readMoreContacts);
    router.get("/contact/read-more-contacts-sent", auth.checkLoggedIn, contact.readMoreContactsSent);
    router.get("/contact/read-more-contacts-received", auth.checkLoggedIn, contact.readMoreContactsReceived);

    //Route of notification
    router.get("/notification/read-more", auth.checkLoggedIn, notification.readMore);
    router.put("/notification/mark-all-read", auth.checkLoggedIn, notification.markAllAsRead);

    //Route of message
    router.post("/message/add-new-text-emoji", auth.checkLoggedIn, messageValid.checkMessageLength, message.addNewTextEmoji);
    router.post("/message/add-new-image", auth.checkLoggedIn, message.addNewImage);
    router.post("/message/add-new-attachment", auth.checkLoggedIn, message.addNewAttachment);

    //Route of group
    router.get("/contact/search-friends/:keyword", auth.checkLoggedIn, contactValid.searchFriends, contact.searchFriends);
    router.post("/group-chat/add-new", auth.checkLoggedIn, groupChatValid.addNewGroup, groupChat.addNewGroup);

    //Route of read more all chat
    router.get("/message/read-more-all-chat", auth.checkLoggedIn, message.readMoreAllChat);
    router.get("/message/read-more-group-chat", auth.checkLoggedIn, message.readMoreGroupChat);
    router.get("/message/read-more-user-chat", auth.checkLoggedIn, message.readMoreUserChat);
    router.get("/message/read-more", auth.checkLoggedIn, message.readMore);



    return app.use("/", router);
}
module.exports = initRoutes;
