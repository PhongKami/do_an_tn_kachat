import express from 'express';
import connectDB from './config/connectDB';
//import ContactModel from './models/contactModel';
import configViewEngine from './config/viewEngine';
import initRoutes from "./routes/web";
import bodyParser from "body-parser";
import connectFlash from "connect-flash";
import session from "./config/session";
import passport from "passport";
import http from "http";
import socketio from "socket.io";
import initSockets from "./sockets/index";
import configSocketIo from "./config/socketio";
import events from "events";
import * as configApp from "./config/app";
import cookieParser from "cookie-parser";

//Sử dụng pem để nó tạo ra chứng chỉ tự kí https. Sau này nếu có host thì không cần
/**==========
 * Cấu hình https ảo
 */
// import pem from "pem";
// import https from "https";
// pem.config({
//   pathOpenSSL: 'C:\\Program Files\\OpenSSL-Win64\\bin\\openssl'
// })
// pem.createCertificate({ days: 1, selfSigned: true }, function (err, keys) {
//     if (err) {
//         throw err;
//     }

//    //Init app
// const app = express();

// //Set max connection event listener
// events.EventEmitter.defaultMaxListeners = configApp.app.max_event_listener; // fix lỗi max số lượng lắng nghe của socket io


// //Init server with socketio & express app
// const server = http.createServer(app);
// const io = socketio(server);

// //Connect to DB
// connectDB();

// //Config sesion
// session.config(app);

// //Config view Engine
// configViewEngine(app);

// //Enable post data for request
// app.use(bodyParser.urlencoded({ extended: true }));

// //Enable flash message
// app.use(connectFlash());

// //Use cookie parser
// app.use(cookieParser());

// //Run env
// require('dotenv').config();

// //Config passport
// app.use(passport.initialize());
// app.use(passport.session());

// //Init all routes
// initRoutes(app);

// //Config for socketio
// configSocketIo(io, cookieParser, session.sessionStore);

// //Init all socket
// initSockets(io);

//     https.createServer({ key: keys.serviceKey, cert: keys.certificate }, app).listen(process.env.APP_PORT, process.env.APP_HOST, () => {
//         console.log(`Server running on post ${process.env.APP_PORT} ...`);
//     })
// })

/**=============
 * Sử dụng pem rồi . sau này bỏ pem đi thì chạy đoạn phía dưới
 */

//Init app
const app = express();

//Set max connection event listener
events.EventEmitter.defaultMaxListeners = configApp.app.max_event_listener; // fix lỗi max số lượng lắng nghe của socket io

//Init server with socketio & express app
const server = http.createServer(app);
const io = socketio(server);

//Connect to DB
connectDB();

//Config sesion
session.config(app);

//Config view Engine
configViewEngine(app);

//Enable post data for request
app.use(bodyParser.urlencoded({ extended: true }));

//Enable flash message
app.use(connectFlash());

//Use cookie parser
app.use(cookieParser());

//Run env
require('dotenv').config();

//Config passport
app.use(passport.initialize());
app.use(passport.session());

//Init all routes
initRoutes(app);

//Config for socketio
configSocketIo(io, cookieParser, session.sessionStore);

//Init all socket
initSockets(io);

// Server Listen at port...
// server.listen(process.env.APP_PORT, process.env.APP_HOST, (err) => { // Sử dụng app.listen cũng được nhưng socket.io không được hỗ trợ
//     if (err)
//         console.log(err);
//     console.log(`Server running on post ${process.env.APP_PORT} ...`);
// });
server.listen(process.env.PORT, (err) => { // Sử dụng app.listen cũng được nhưng socket.io không được hỗ trợ
    if (err)
        console.log(err);
    console.log(`Server running on post ${process.env.PORT} ...`);
});
