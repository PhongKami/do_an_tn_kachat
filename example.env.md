//Cấu hình biến môi trường database nếu đổi máy
DB_CONNECTION=***
DB_HOST=***
DB_PORT=***
DB_NAME=***
DB_USERNAME=***
DB_PASSWORD=***

//Cấu hình host và port server 
APP_HOST=***
APP_PORT=***

//Cấu hình key và secret
SECTION_KEY=***
SECTION_SECRET=***

//Cấu hình host và port mail admin 
MAIN_USER=***
MAIL_PASSWORD=***
MAIL_HOST=***
MAIL_PORT=***

//Cấu hình facebook
FB_APP_ID=***
FB_APP_SECRET=***
FB_CALLBACK_URL=***

//Cấu hình google
GG_APP_ID=***
GG_APP_SECRET=***
GG_CALLBACK_URL=***

